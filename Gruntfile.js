module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    sass: {
      dist: {
        files: [{
          expand: true,
          cwd: 'assets/src/scss',
          src: ['*.scss'],
          dest: 'assets/css',
          ext: '.css'
        }],
        options: {
          banner: '/* F3 Based Ecommerce Shop\n * Jamie Watson \n * http://www.jamiewatson.me\n */'
        }
      }
    },
    uglify: {
      all: {
        files: {
          'assets/js/app.min.js': [
            'assets/src/js/app.js'
          ],
          'assets/js/billing.min.js': [
            'assets/src/js/billing.js'
          ]
        }
      }
    },
    watch: {
      options:{
        livereload: 1337,
      },
      js: {
        files: ['assets/src/js/*.js'],
        tasks: ['uglify']
      },
      css: {
        files: ['assets/src/scss/**/*.scss'],
        tasks: ['sass:dist']
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-sass');

  grunt.registerTask('default', ['sass:dist','uglify','watch']);

};