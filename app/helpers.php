<?php


if( !function_exists('dd') ){
	
	/**
	 * Die and dump a variable in a preformatted tag for debugging / dev purposes
	 * 
	 * @param  $var - Variable to be output
	 */
	function dd( $var ){

		echo "<pre>";
		die( var_dump( $var ) );
		
	}

}

if( !function_exists('product_image_url') ){
	
	/**
	 * Simple helper method to generate a product image URL
	 * 
	 * @param  String $file Name of the file on the system
	 * @param  String $size Size of the image to be output (ie. 100x100)
	 * 
	 * @return String formatted string of the image url in the system
	 */
	
	function product_image_url( $file, $size ){
		return '/assets/images/products/' . $file . '_' . $size . '.jpg';
	}

}


if( !function_exists('resource_routes')){

	/**
	 * A function to generate RESTful resource route definitions in the F3 Framework
     * 
	 * @param  string $resource   name of the singular resource ( eg. product )
	 * @param  string $controller name of the hanglind controller
	 * @param  string $prefix     if these routes should be prefixed with a particular string ( eg. admin )
	 */
	function resource_routes( $resource, $controller, $prefix = '' ){

		$f3=Base::instance();

		$prefix = $prefix?'/'.$prefix:'';

		$f3->route('GET @'.$resource.'_index: '. $prefix.'/'.$resource, $controller . '->index');
		$f3->route('GET @'.$resource.'_create: '. $prefix.'/'.$resource.'/create', $controller . '->create');
		$f3->route('GET @'.$resource.'_show: '.	$prefix.'/'.$resource.'/@id', $controller . '->show');
		$f3->route('GET @'.$resource.'_edit: '.	$prefix.'/'.$resource.'/@id/edit', $controller . '->edit');
		$f3->route('POST @'.$resource.'_store: '. $prefix.'/'.$resource.'', $controller . '->store');
		$f3->route('POST @'.$resource.'_update: '.$prefix.'/'.$resource.'/@id/edit', $controller . '->update');
		$f3->route('DELETE @'.$resource.'_delete: '.$prefix.'/'.$resource.'/@id', $controller . '->delete');
	}
}