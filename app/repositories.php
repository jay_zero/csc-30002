<?php

// Register our Repositories in the IoC Container such that if we want to swap out the database / mapper this could be done with relative ease
// One thing to note: It might be worth having some kind of provider for this.
// 
// Used repositories as an additional layer between the controller and the model / datasource

$repositories = array(
	'\v1l85\Repositories\OrderRepositoryInterface'			=>	'\v1l85\Repositories\F3\OrderRepository',
	'\v1l85\Repositories\OrderLineRepositoryInterface'		=>	'\v1l85\Repositories\F3\OrderLineRepository',
	'\v1l85\Repositories\CategoryRepositoryInterface'		=>	'\v1l85\Repositories\F3\CategoryRepository',
	'\v1l85\Repositories\ProductRepositoryInterface'		=>	'\v1l85\Repositories\F3\ProductRepository',
	'\v1l85\Repositories\ProductImageRepositoryInterface'	=>	'\v1l85\Repositories\F3\ProductImageRepository',
	'\v1l85\Repositories\CustomerRepositoryInterface'		=>	'\v1l85\Repositories\F3\CustomerRepository',
	'\v1l85\Repositories\CustomerAddressRepositoryInterface'=>	'\v1l85\Repositories\F3\CustomerAddressRepository',
	'\v1l85\Repositories\AdminUserRepositoryInterface'		=>	'\v1l85\Repositories\F3\AdminUserRepository',
	'\v1l85\Repositories\PageRepositoryInterface'			=>	'\v1l85\Repositories\F3\PageRepository'
);

foreach( $repositories as $interface => $repository ){
	$container->bind( $interface, $repository );
}