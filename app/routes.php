<?php


/**
 * 	FRONT END ROUTES
 */


//Home and information pages
$f3->route('GET @home: /', 'v1l85\Controllers\PageController->home');
$f3->route('GET /@slug', 'v1l85\Controllers\PageController->page');


//Authorisation Routes
$f3->route('GET @logout: /logout', 'v1l85\Controllers\SessionController->delete');
$f3->route('GET @login: /login', 'v1l85\Controllers\SessionController->index');
$f3->route('POST @login: /login', 'v1l85\Controllers\SessionController->create');

$f3->route('POST @register: /register', 'v1l85\Controllers\UserController->create');


//Categories
$f3->route('GET /category/@slug', 'v1l85\Controllers\CategoryController->show');
$f3->route('GET /category/@slug/page/@page', 'v1l85\Controllers\CategoryController->show');

//Product Routes
$f3->route('GET /product/@slug [ajax]', 'v1l85\Controllers\ProductController->showAjax');
$f3->route('GET @product: /product/@slug [sync]', 'v1l85\Controllers\ProductController->show');

$f3->route('POST @search: /search', 'v1l85\Controllers\SearchController->index');


$f3->route('POST @register: /register', 'v1l85\Controllers\CustomerController->store');
$f3->route('GET @register: /register', 'v1l85\Controllers\CustomerController->create');

//Basket Routes
$f3->route('POST @basket_create: /basket/add', 'v1l85\Controllers\BasketController->create');
$f3->route('POST @basket_update: /basket/update', 'v1l85\Controllers\BasketController->update');
$f3->route('GET @basket_empty: /basket/empty', 'v1l85\Controllers\BasketController->delete');
$f3->route('GET @basket_remove: /basket/remove/@id', 'v1l85\Controllers\BasketController->remove');
$f3->route('GET @basket_index: /basket [ajax]', 'v1l85\Controllers\BasketController->index');
$f3->route('GET @basket_index: /basket [sync]', 'v1l85\Controllers\BasketController->show');


$f3->route('GET @checkout_index: /checkout', 'v1l85\Controllers\CheckoutController->index'); 
$f3->route('POST @checkout_store: /checkout', 'v1l85\Controllers\CheckoutController->store'); 
$f3->route('GET @checkout_success: /checkout/success', 'v1l85\Controllers\CheckoutController->show'); 


/**
 * 	BACK END / ADMIN ROUTES
 */

$f3->route('GET /admin', 'v1l85\Controllers\Admin\DashboardController->index');

$f3->route('GET /admin/logout', 'v1l85\Controllers\Admin\SessionController->delete');
$f3->route('GET /admin/login', 'v1l85\Controllers\Admin\SessionController->index');
$f3->route('POST /admin/login', 'v1l85\Controllers\Admin\SessionController->create');

$f3->route('GET /admin/image/delete/@id', 'v1l85\Controllers\Admin\ProductImageController->delete');

resource_routes( 'category', 'v1l85\Controllers\Admin\CategoryController', 'admin' );
resource_routes( 'product', 'v1l85\Controllers\Admin\ProductController', 'admin' );
resource_routes( 'order', 'v1l85\Controllers\Admin\OrderController', 'admin' );
resource_routes( 'page', 'v1l85\Controllers\Admin\PageController', 'admin' );

//I make the distinction between customer and admin user types.  They could have gone into the same table but that would require setting up
//User Groups and User Permissions also.
resource_routes( 'customer', 'v1l85\Controllers\Admin\CustomerController', 'admin' );
resource_routes( 'user', 'v1l85\Controllers\Admin\UserController', 'admin' );



/**
 * 	V1 API ROUTES
 */

$f3->route('GET /api/v1/category', 'v1l85\Controllers\Api\CategoryApiController->index');
$f3->route('GET /api/v1/category/@id', 'v1l85\Controllers\Api\CategoryApiController->show');

$f3->route('GET /api/v1/product', 'v1l85\Controllers\Api\ProductApiController->index');
$f3->route('GET /api/v1/product/@id', 'v1l85\Controllers\Api\ProductApiController->show');

//Use the same controller for the basket api since this was written in a sort of Api anyway
$f3->route('POST /api/v1/basket/add', 'v1l85\Controllers\BasketController->create');
$f3->route('POST /api/v1/basket/update', 'v1l85\Controllers\BasketController->update');
$f3->route('GET /api/v1/basket/empty', 'v1l85\Controllers\BasketController->delete');
$f3->route('GET /api/v1/basket/remove/@id', 'v1l85\Controllers\BasketController->remove');
$f3->route('GET /api/v1/basket', 'v1l85\Controllers\BasketController->index');

// Used to allow the storage of orders
$f3->route('POST /api/v1/order', 'v1l85\Controllers\Api\OrderApiController->store');