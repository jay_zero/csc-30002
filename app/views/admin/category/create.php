<form action="{{ 'category_store' | alias }}" method="post">

    <div class="panel panel-default">
        <div class="panel-heading">
            Create Category
        </div>

        <div class="panel-body">

            <check if="{{ isset(@SESSION.errors) }}">
                <div class="alert alert-danger">
                    <repeat group="{{ @SESSION.errors }}" value="@error">
                        <p>{{ @error | raw }}</p>
                    </repeat>
                </div>
            </check>


            <div class="form-group">
                <label>Category Name</label>
                <input class="form-control" name="name" value="{{ isset(@POST.name)?@POST.name:'' }}"> 
            </div>

            <div class="form-group">
                <label>Parent Category</label>
                <select class="form-control" name="parent_id"> 
                    <option value="0">Top Level Category</option>
                    <repeat group="@categories" value="@category">
                        <option value="{{ @category->category_id }}" {{ isset(@POST.parent_id)&&@POST.parent_id==@category->category_id?'selected':'' }}>{{ @category->name }}</option>
                    </repeat>
                </select>
            </div>

            <div class="form-group">
                <label>SEO Friendly URL</label>
                <input class="form-control" name="slug" value="{{ isset(@POST.slug)?@POST.slug:'' }}">
                <p class="help-block">Separate keywords with '-' for example <code>bathroom-and-kitchen</code></p>    
            </div>

            <div class="form-group">
                <label>Description</label>
                <textarea class="form-control" rows="3" name="description">{{ isset(@POST.description)?@POST.description:'' }}</textarea>
            </div>
        </div>

        <div class="panel-footer">
            <button class="btn btn-success" type="submit">Save</button>
        </div>
    </div>

</form>