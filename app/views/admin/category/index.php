
<div class="panel panel-default">
	<div class="panel-heading">
		Categories
		<a href="{{ 'category_create' | alias }}" class="btn btn-primary btn-sm pull-right">Create</a>
	</div>
	<div class="panel-body">

		<table class="table table-striped">
			<thead>
				<th>ID</th>
				<th>Category Name</th>
				<th>Category Slug</th>
				<th>Actions</th>
			</thead>
			<tbody>
			
			<repeat group="{{ @categories }}" value="{{ @category }}">
				<tr>
					<td>{{ @category->category_id }}</td>
					<td>{{ @category->name }}</td>
					<td>{{ @category->slug }}</td>
					<td>
						<a href="{{ 'category_edit', 'id='.@category->category_id | alias }}" class="btn btn-primary">Edit</a>
						<a href="javascript:void(0);" class="btn btn-danger btn-delete" data-id="{{ @category->category_id }}">Delete</a>
					</td>
				</tr>
			</repeat>

			</tbody>
		</table>
	</div>
</div>



<script>

$(function(){

	$('.btn-delete').on('click', function(e){
		var _this = $(this);
        $.ajax({
        	//A little too hacky for my liking.  Change this when possible.
            url: '{{ 'category_delete', 'id=\'+$(this).data(\'id\')' | alias }},
            type: 'DELETE',
            success: function( data ){
            	//If data.success :]
            	_this.parents('tr').remove();
            }
        });

        return false;
    });

})
    

</script>