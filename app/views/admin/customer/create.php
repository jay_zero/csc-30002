<form action="{{ 'customer_store' | alias }}" method="post">

    <div class="panel panel-default">
        <div class="panel-heading">
            Create Customer
        </div>

        <div class="panel-body">

            <check if="{{ isset(@SESSION.errors) }}">
                <div class="alert alert-danger">
                    <repeat group="{{ @SESSION.errors }}" value="@error">
                        <p>{{ @error | raw }}</p>
                    </repeat>
                </div>
            </check>
            
            <div class="form-group">
                <label>First Name</label>
                <input class="form-control" name="firstname" value="{{ isset(@POST.firstname)?@POST.firstname:'' }}"> 
            </div>

            <div class="form-group">
                <label>Last Name</label>
                <input class="form-control" name="lastname" value="{{ isset(@POST.lastname)?@POST.lastname:'' }}"> 
            </div>

            <div class="form-group">
                <label>Password</label>
                <input class="form-control" name="password" type="password"> 
            </div>

            <div class="form-group">
                <label>Email</label>
                <input class="form-control" name="email"  value="{{ isset(@POST.email)?@POST.email:'' }}"> 
            </div>

        </div>

        <div class="panel-footer">
            <button class="btn btn-success" type="submit">Save</button>
        </div>
    </div>

</form>