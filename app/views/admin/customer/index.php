
<div class="panel panel-default">
	<div class="panel-heading">
		Customers
		<a href="{{ 'customer_create' | alias }}" class="btn btn-primary btn-sm pull-right">Create</a>
	</div>
	<div class="panel-body">

		<table class="table table-striped">
			<thead>
				<th>ID</th>
				<th>Customer Name</th>
				<th>Customer Email</th>
				<th>Actions</th>
			</thead>
			<tbody>
			
			<repeat group="{{ @customers }}" value="{{ @customer }}">
				<tr>
					<td>{{ @customer->customer_id }}</td>
					<td>{{ @customer->firstname }} {{ @customer->lastname }}</td>
					<td>{{ @customer->email }}</td>
					<td>
						<a href="{{ 'customer_edit', 'id='.@customer->customer_id | alias }}" class="btn btn-primary">Edit</a>
						<a href="javascript:void(0);" class="btn btn-danger btn-delete" data-id="{{ @customer->customer_id }}">Delete</a>
					</td>
				</tr>
			</repeat>

			</tbody>
		</table>
	</div>
</div>



<script>

$(function(){

	$('.btn-delete').on('click', function(e){
		var _this = $(this);
        $.ajax({
        	//A little too hacky for my liking.  Change this when possible.
            url: '{{ 'customer_delete', 'id=\'+$(this).data(\'id\')' | alias }},
            type: 'DELETE',
            success: function( data ){
            	//If data.success :]
            	_this.parents('tr').remove();
            }
        });

        return false;
    });

})
    

</script>