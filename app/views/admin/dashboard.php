<h1>Admin Dashboard</h1>

<div class="row">
    <div class="col-md-4">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-shopping-cart fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{ @orderstats.num }}</div>
                        <div>Total Orders</div>
                    </div>
                </div>
            </div>
            <a href="/admin/order">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-bar-chart fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{ @orderstats.total | currency }}</div>
                        <div>Total Sales</div>
                    </div>
                </div>
            </div>
            <a href="/admin/order">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-users fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{ @customers.total }}</div>
                        <div>Customers</div>
                    </div>
                </div>
            </div>
            <a href="/admin/customer">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		Sales in {{ date('F') }}
	</div>
	
	<div class="panel-body">
		<div id="sales_per_day" style="height:300px;"></div>
	</div>
</div>




<script>
	var sales = [];

	<repeat group="{{ @sales }}" key="{{ @time }}" value="{{ @total }}">
		sales.push( [{{ @time*1000 }}, {{ @total/100 }}] );
	</repeat>

	var options = {
		xaxis: {
			mode: "time",
			tickSize: [1, "day"] 
		},
		series: {
			lines: { show: true },
			points: { show: true }
		},
		grid: {
			hoverable: true
		},
		colors: ["#EE3F40"],
		shadowSize: 0
	};

	$(function(){

		$("<div id='tooltip'></div>").css({
			position: "absolute",
			display: "none",
			border: "1px solid #fdd",
			padding: "2px",
			"background-color": "#fee",
			opacity: 0.80
		}).appendTo("body");
		
		$.plot('#sales_per_day', [{ data: sales, label: 'Number of Sales', lines: {show: true, } }], options);

		$("#sales_per_day").bind("plothover", function (event, pos, item) {
			if (item) {
				var x = item.datapoint[0].toFixed(2),
					y = item.datapoint[1].toFixed(2);

				$("#tooltip").html(item.series.label + " £" + y)
					.css({top: item.pageY+5, left: item.pageX+5})
					.fadeIn(200);
			} else {
				$("#tooltip").hide();
			}
		});

	})
</script>