<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<title>Admin Panel</title>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

	<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0-beta.3/css/select2.min.css" rel="stylesheet" />

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

	<style>
	.sidebar {
    position: fixed;
    top: 51px;
    bottom: 0;
    left: 0;
    z-index: 1000;
    display: block;
    padding: 20px;
    overflow-x: hidden;
    overflow-y: auto; /* Scrollable contents if viewport is shorter than content. */
    background-color: #f5f5f5;
    border-right: 1px solid #eee;
  }

  .nav-sidebar {
  margin-right: -21px; /* 20px padding + 1px border */
  margin-bottom: 20px;
  margin-left: -20px;
}
.nav-sidebar > li > a {
  padding-right: 20px;
  padding-left: 20px;
}
.nav-sidebar > .active > a,
.nav-sidebar > .active > a:hover,
.nav-sidebar > .active > a:focus {
  color: #fff;
  background-color: #428bca;
}
.huge {
font-size: 40px;
}
.main{
padding:30px;}
.navbar-static-top{ margin: 0; }
h1{
	margin: 0 0 20px 0; }</style>
	

</head>
<body>

		<nav class="navbar navbar-default navbar-static-top">
			<div class="container-fluid">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			    <span class="sr-only">Toggle navigation</span>
			    <span class="icon-bar"></span>
			    <span class="icon-bar"></span>
			    <span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="#">F3Commerce Admin</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			  <ul class="nav navbar-nav navbar-right">
			  <li><a href="/" role="button" aria-expanded="false">Site Home</a></li>
			    <li class="dropdown">
			      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ @user.username }} <span class="caret"></span></a>
			      <ul class="dropdown-menu" role="menu">
			        <li><a href="/admin/logout">Logout</a></li>
			      </ul>
			    </li>
			  </ul>
			</div>
			</div>
		</nav>

		 <div class="container-fluid">
	      <div class="row">
	        <div class="col-sm-3 col-md-2 sidebar">
				<ul class="nav nav-sidebar">
					<li class="active"><a href="/admin">Dashboard <span class="sr-only">(current)</span></a></li>
					<li><a href="/admin/product">Products</a></li>
					<li><a href="/admin/category">Categories</a></li>
					<li><a href="/admin/order">Orders</a></li>
					<li><a href="/admin/page">Custom Pages</a></li>
					<li><a href="/admin/customer">Customers</a></li>
					<li><a href="/admin/user">Admin Users</a></li>
				</ul>
       		</div>

			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				{{ @content | raw }}
			</div>
		</div>
		</div>

		
		<script language="javascript" type="text/javascript" src="/assets/js/charts/jquery.flot.min.js"></script>
		<script language="javascript" type="text/javascript" src="/assets/js/charts/jquery.flot.time.min.js"></script>
		<script language="javascript" type="text/javascript" src="/assets/js/charts/jquery.flot.resize.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0-beta.3/js/select2.min.js"></script>
		<script>
			$(function(){
				$('select').select2();
			});
		</script>

</body>
</html>