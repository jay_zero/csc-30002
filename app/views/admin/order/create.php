<form action="{{ 'user_store' | alias }}" method="post">

    <div class="panel panel-default">
        <div class="panel-heading">
            Create Admin User
        </div>

        <div class="panel-body">
            <div class="form-group">
                <label>Username</label>
                <input class="form-control" name="username"> 
            </div>

            <div class="form-group">
                <label>Password</label>
                <input class="form-control" name="password"> 
            </div>

            <div class="form-group">
                <label>Email</label>
                <input class="form-control" name="email"> 
            </div>

        </div>

        <div class="panel-footer">
            <button class="btn btn-success" type="submit">Save</button>
        </div>
    </div>

</form>