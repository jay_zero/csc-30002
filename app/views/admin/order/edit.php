<form action="{{ 'category_update', 'id='.@PARAMS.id | alias }}" method="post">

    <div class="panel panel-default">
        <div class="panel-heading">
            Create Category
        </div>

        <div class="panel-body">
            <div class="form-group">
                <label>Category Name</label>
                <input class="form-control" name="name" value="{{ @POST.name }}"> 
            </div>

            <div class="form-group">
                <label>Parent Category</label>
                <select class="form-control" name="parent_id"> 
                    <option value="0">Top Level Category</option>
                </select>
            </div>

            <div class="form-group">
                <label>SEO Friendly URL</label>
                <input class="form-control" name="slug" value="{{ @POST.slug }}">
                <p class="help-block">Separate keywords with '-' for example <code>bathroom-and-kitchen</code></p>    
            </div>

            <div class="form-group">
                <label>Description</label>
                <textarea class="form-control" rows="3" name="description">{{ @POST.description }}</textarea>
            </div>
        </div>

        <div class="panel-footer">
            <button class="btn btn-success" type="submit">Save</button>
        </div>
    </div>

</form>