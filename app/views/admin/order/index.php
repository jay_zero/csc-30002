
<div class="panel panel-default">
	<div class="panel-heading">
		Orders
	</div>
	<div class="panel-body">

		<table class="table table-striped">
			<thead>
				<th>ID</th>
				<th>Customer</th>
				<th>Date Placed</th>
				<th>Stripe Token</th>
				<th>Order Total</th>
				<th>Actions</th>
			</thead>
			<tbody>
			
			<repeat group="{{ @orders }}" value="{{ @order }}">
				<tr>
					<td>{{ @order->order_id }}</td>
					<td>
						<check if="{{ @order->customer_id }}">
							<true>
								<a href="{{ 'customer_edit', 'id='.@order->customer_id | alias }}">{{ @order->billing_firstname }} {{ @order->billing_lastname }}</a>
							</true>
							<false>{{ @order->billing_firstname }} {{ @order->billing_lastname }}</false>
						</check>
					</td>
					<td>{{ date('d/m/Y', @order->date_placed) }}</td>
					<td>{{ @order->stripe_token }}</td>
					<td>{{ @order->order_total | currency }}</td>
					<td>
						<!-- <a href="{{ 'order_edit', 'id='.@order->order_id | alias }}" class="btn btn-primary btn-sm">Change Order Status</a> -->
						<a href="{{ 'order_show', 'id='.@order->order_id | alias }}" class="btn btn-success btn-sm">View Order</a>
					</td>
				</tr>
			</repeat>

			</tbody>
		</table>
	</div>
</div>



<script>

$(function(){

	$('.btn-delete').on('click', function(e){
		var _this = $(this);
        $.ajax({
        	//A little too hacky for my liking.  Change this when possible.
            url: '{{ 'order_delete', 'id=\'+$(this).data(\'id\')' | alias }},
            type: 'DELETE',
            success: function( data ){
            	//If data.success :]
            	_this.parents('tr').remove();
            }
        });

        return false;
    });

})
    

</script>