<div class="row">
	<div class="col-md-4">
		<div class="panel panel-default" style="min-height: 200px">
			<div class="panel-heading">
				Order Summary
			</div>
			<div class="panel-body">
				<strong>Total Spent:</strong> {{ @order->order_total | currency }}<br />
				<strong>Stripe Token:</strong> {{ @order->stripe_token }}<br />
				<strong>VAT Rate:</strong> {{ (@order->vat_rate-1)*100 }}%<br />
				<strong>Order Status:</strong> <span class="label label-success">Pending</span><br />
				<check if="{{ @order->customer_id }}">
					<strong>Customer:</strong> <a href="{{ 'customer_edit', 'id='.@order->customer_id | alias }}">{{ @order->billing_firstname }} {{ @order->billing_lastname }}</a><br />
				</check>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="panel panel-default" style="min-height: 200px">
			<div class="panel-heading">
				Billing Address
			</div>
			<div class="panel-body">
				<strong>{{ @order->billing_firstname }} {{ @order->billing_lastname }}</strong><br />
				{{ @order->billing_line_1 }}<br />
				{{ @order->billing_line_2 }}<br />
				{{ @order->billing_city }}<br />
				{{ @order->billing_postcode }}<br />
			</div>
		</div>
	</div>

	<div class="col-md-4">
		<div class="panel panel-default" style="min-height: 200px">
			<div class="panel-heading">
				Shipping Address
			</div>
			<div class="panel-body">
				<strong>{{ @order->shipping_firstname }} {{ @order->shipping_lastname }}</strong><br />
				{{ @order->shipping_line_1 }}<br />
				{{ @order->shipping_line_2 }}<br />
				{{ @order->shipping_city }}<br />
				{{ @order->shipping_postcode }}<br />
			</div>
		</div>
	</div>
</div>

<div class="panel panel-primary">
	<div class="panel-heading">
		Items Ordered
	</div>
	<div class="panel-body">
		<include href="admin/order/summary.php" with="order={{@order}}" />
	</div>
</div>