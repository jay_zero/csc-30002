<table class="basket table table-striped">
	<thead>
	<tr>
		<th>Item</th>
		<th>Qty</th>
		<th>VAT?</th>
		<th>Price</th>
		<th class="text-right">Total</th>
	</tr>
	</thead>
	<tbody>
		<repeat group="{{ @order->lines }}" value="{{ @item }}">
			<tr>
				<td>{{ @item.product_name }}</td>
				<td>{{ @item.quantity }}</td>
				<td>
					<check if="{{ @item.charge_vat }}">
						<true><i class="fa fa-check"></i></true>
						<false><i class="fa fa-times"></i></false>
					</check>
				</td>
				<td>
					<check if="{{ @item.charge_vat }}">
						<true>{{ @item.price * @order->vat_rate | currency }}</true>
						<false>{{ @item.price | currency }}</false>
					</check>
				</td>
				<td class="text-right">
					<check if="{{ @item.charge_vat }}">
						<true>{{ @item.price * @item.quantity * @order->vat_rate | currency }}</true>
						<false>{{ @item.price  * @item.quantity | currency }}</false>
					</check>
				</td>
			</tr>	
		</repeat>
	</tbody>
</table>

<table class="table subtotal">
	<tr>
		<th class="text-right">Sub-Total:</th>
		<td class="text-right"></td>
	</tr>
	<tr>
		<th class="text-right">Tax:</th>
		<td class="text-right"></td>
	</tr>
	<tr>
		<th class="text-right">Total:</th>
		<td class="text-right">{{ @order->order_total | currency }}</td>
	</tr>
</table>

<div class="clearfix"></div>
