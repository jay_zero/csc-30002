<form action="{{ 'page_update' | alias }}" method="post">

    <div class="panel panel-default">
        <div class="panel-heading">
            Edit Page
        </div>

        <div class="panel-body">

            <check if="{{ isset(@SESSION.errors) }}">
                <div class="alert alert-danger">
                    <repeat group="{{ @SESSION.errors }}" value="@error">
                        <p>{{ @error | raw }}</p>
                    </repeat>
                </div>
            </check>


            <div class="form-group">
                <label>Page Name</label>
                <input class="form-control" name="name" value="{{ isset(@POST.name)?@POST.name:'' }}"> 
            </div>

            <div class="form-group">
                <label>SEO Friendly URL</label>
                <input class="form-control" name="slug" value="{{ isset(@POST.slug)?@POST.slug:'' }}">
                <p class="help-block">Separate keywords with '-' for example <code>bathroom-and-kitchen</code></p>    
            </div>

            <div class="form-group">
                <label>Content</label>
                <textarea class="form-control" rows="10" name="content">{{ isset(@POST.content)?@POST.content:'' }}</textarea>
                <p class="help-block">Markdown is supported.</p>
            </div>
        </div>

        <div class="panel-footer">
            <button class="btn btn-success" type="submit">Save</button>
        </div>
    </div>

</form>