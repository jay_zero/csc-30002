
<div class="panel panel-default">
	<div class="panel-heading">
		Custom Pages
		<a href="{{ 'page_create' | alias }}" class="btn btn-primary btn-sm pull-right">Create</a>
	</div>
	<div class="panel-body">

		<table class="table table-striped">
			<thead>
				<th>ID</th>
				<th>Page Name</th>
				<th>Page Slug</th>
				<th>Actions</th>
			</thead>
			<tbody>
			
			<repeat group="{{ @pages }}" value="{{ @page }}">
				<tr>
					<td>{{ @page->page_id }}</td>
					<td>{{ @page->name }}</td>
					<td>{{ @page->slug }}</td>
					<td>
						<a href="{{ 'page_edit', 'id='.@page->page_id | alias }}" class="btn btn-primary">Edit</a>
						<a href="javascript:void(0);" class="btn btn-danger btn-delete" data-id="{{ @page->page_id }}">Delete</a>
					</td>
				</tr>
			</repeat>

			</tbody>
		</table>
	</div>
</div>



<script>

$(function(){

	$('.btn-delete').on('click', function(e){
		var _this = $(this);
        $.ajax({
        	//A little too hacky for my liking.  Change this when possible.
            url: '{{ 'page_delete', 'id=\'+$(this).data(\'id\')' | alias }},
            type: 'DELETE',
            success: function( data ){
            	//If data.success :]
            	_this.parents('tr').remove();
            }
        });

        return false;
    });

})
    

</script>