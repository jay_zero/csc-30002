<form action="{{ 'product_update' | alias }}" method="post" enctype="multipart/form-data">

    <div class="panel panel-default">
        <div class="panel-heading">
            Edit Product
        </div>

        <div class="panel-body">


            <check if="{{ isset(@SESSION.errors) }}">
                <div class="alert alert-danger">
                    <repeat group="{{ @SESSION.errors }}" value="@error">
                        <p>{{ @error | raw }}</p>
                    </repeat>
                </div>
            </check>

            <div role="tabpanel">

                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#details" aria-controls="details" role="tab" data-toggle="tab">Basic Details</a></li>
                    <li role="presentation"><a href="#price" aria-controls="price" role="tab" data-toggle="tab">Price</a></li>
                    <li role="presentation"><a href="#stock" aria-controls="stock" role="tab" data-toggle="tab">Stock</a></li>
                    <li role="presentation"><a href="#images" aria-controls="images" role="tab" data-toggle="tab">Images</a></li>
                    <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane active" id="details">

                        <div class="form-group">
                            <label>Product Name</label>
                            <input class="form-control" name="name" value="{{ @POST.name }}"> 
                        </div>

                        <div class="form-group">
                            <label>SKU</label>
                            <input class="form-control" name="sku" value="{{ @POST.sku }}"> 
                        </div>

                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" rows="3" name="description">{{ @POST.description }}</textarea>
                        </div>

                        <div class="form-group">
                            <label>Categories</label>
                            <select class="form-control" name="category_id[]" multiple="multiple"> 
                                <repeat group="@categories" value="@category">
                                    <option value="{{ @category->category_id }}" {{ in_array(@category->category_id, @category_list)?'selected':'' }}>{{ @category->name }}</option>
                                </repeat>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>SEO Friendly URL</label>
                            <input class="form-control" name="slug" value="{{ @POST.slug }}">
                            <p class="help-block">Separate keywords with '-' for example <code>bathroom-and-kitchen</code></p>    
                        </div>

                    </div>

                    <div role="tabpanel" class="tab-pane" id="price">
                        
                        <div class="form-group">
                            <label>Price</label>
                            <input class="form-control" name="price" value="{{ @POST.price }}">  
                        </div>

                        <div class="form-group">
                            <label>Special Price</label>
                            <input class="form-control" name="special_price" value="{{ @POST.special_price }}">  
                        </div>

                    </div>

                    <div role="tabpanel" class="tab-pane" id="stock">
                        
                        <div class="form-group">
                            <label>Stock Level</label>
                            <input class="form-control" name="stock_level" value="{{ @POST.stock_level }}">  
                        </div>
                 

                        <div class="form-group">
                            <label>Out of Stock Status</label>
                            <select class="form-control" name="out_of_stock_status" style="width:100%"> 
                                <exclude><repeat group="@categories" value="@category">
                                    <option value="{{ @category->category_id }}">{{ @category->name }}</option>
                                </repeat></exclude>
                                <option value="0">Out of Stock</option>
                                <option value="1">Pre-Order</option>
                                <option value="2">In Stock</option>
                            </select>
                        </div>

                    </div>

                    <div role="tabpanel" class="tab-pane" id="images">

                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Default?</th>
                                    <th colspan="2">Image File</th>
                                    <th>Caption</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <repeat group="@images" value="@image">
                                    <tr>
                                        <td><input type="radio" name="default_image" value="{{ @image->product_image_id }}" {{ @image->product_image_id==@POST.default_image_id?'checked':'' }} /></td>
                                        <td style="width:50px"><img src="/assets/images/products/{{ @image->file }}_100x100.jpg" style="width:100%!important"/></td>
                                        <td><input type="file" name="image[{{ @image->product_image_id }}]" /></td>
                                        <td><input type="text" class="form-control" name="caption[{{ @image->product_image_id }}]" /></td>
                                        <td><a href="javascript:void(0)" class="btn-remove btn btn-xs btn-danger">Remove Image</a></td>
                                    </tr>
                                </repeat>
                            </tbody>
                        </table>

                        <a class="btn btn-primary btn-xs btn-add-image">Add New</a>

                    </div>

                    <div role="tabpanel" class="tab-pane" id="settings">
                        
                        <div class="checkbox">
                            <label><input type="checkbox" name="charge_vat" value="1" {{ @POST.charge_vat?'checked':'' }}> Charge VAT?</label>
                        </div>

                        <div class="checkbox">
                            <label><input type="checkbox" name="featured" value="1" {{ @POST.featured?'checked':'' }}> Featured?</label>
                        </div>

                        <div class="checkbox">
                            <label><input type="checkbox" name="visible" value="1" {{ @POST.visible?'checked':'' }}> Active?</label>
                        </div>

                    </div>

                </div>

            </div>

            
        </div>

        <div class="panel-footer">
            <button class="btn btn-success" type="submit">Save</button>
        </div>
    </div>

</form>


<script type="text/html" id="imagerow">
    <tr>
        <td><input type="radio" name="default_image" value="{ID}" /></td>
        <td><input type="file" name="image[{ID}]" /></td>
        <td><input type="text" class="form-control" name="caption[{ID}]" /></td>
        <td><a href="javascript:void(0)" class="btn-remove btn btn-xs btn-danger">Remove Image</a></td>
    </tr>
</script>


<script>
var counter = -1;
$(function(){
    $('#myTab a').click(function (e) {
      e.preventDefault()
      $(this).tab('show')
    });


    $('.btn-add-image').on( 'click', function(){
        addRow( counter-- );
    });

    $('#images tbody').delegate( '.btn-remove', 'click', function(){
        if( $(this).parents('tr').find('input[name="default_image"]').val() > 0 ){
            $.get('/admin/image/delete/'+$(this).parents('tr').find('input[name="default_image"]').val());
            console.log('deleted');
        }
        $(this).parents('tr').remove();
    })

});

var addRow = function( id ){
    $_row = $('#imagerow').clone().html().replace(/{ID}/g, id);
    $('#images tbody').append( $_row );
}
</script>