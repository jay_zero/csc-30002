
<div class="panel panel-default">
	<div class="panel-heading">
		Products
		<a href="{{ 'product_create' | alias }}" class="btn btn-primary btn-sm pull-right">Create</a>
	</div>
	<div class="panel-body">

		<table class="table table-striped">
			<thead>
				<th>ID</th>
				<th>Product Name</th>
				<th>SKU</th>
				<th>Views</th>
				<th>Stock Level</th>
				<th>Actions</th>
			</thead>
			<tbody>
			
			<repeat group="{{ @products }}" value="{{ @product }}">
				<tr>
					<td>{{ @product->product_id }}</td>
					<td>{{ @product->name }}</td>
					<td>{{ @product->sku }}</td>
					<td>{{ @product->views }}</td>
					<td>{{ @product->stock_level }}</td>
					<td>
						<a href="{{ 'product_edit', 'id='.@product->product_id | alias }}" class="btn btn-primary">Edit</a>
						<a href="javascript:void(0);" class="btn btn-danger btn-delete" data-id="{{ @product->product_id }}">Delete</a>
					</td>
				</tr>
			</repeat>

			</tbody>
		</table>
	</div>
</div>



<script>

$(function(){

	$('.btn-delete').on('click', function(e){
		var _this = $(this);
        $.ajax({
        	//A little too hacky for my liking.  Change this when possible.
            url: '{{ 'product_delete', 'id=\'+$(this).data(\'id\')' | alias }},
            type: 'DELETE',
            success: function( data ){
            	//If data.success :]
            	_this.parents('tr').remove();
            }
        });

        return false;
    });

})
    

</script>