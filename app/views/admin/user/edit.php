<form action="{{ 'user_update' | alias }}" method="post">

    <div class="panel panel-default">
        <div class="panel-heading">
            Edit Admin User
        </div>

        <div class="panel-body">

            <check if="{{ isset(@SESSION.errors) }}">
                <div class="alert alert-danger">
                    <repeat group="{{ @SESSION.errors }}" value="@error">
                        <p>{{ @error | raw }}</p>
                    </repeat>
                </div>
            </check>

            <div class="form-group">
                <label>Username</label>
                <input class="form-control" name="username" value="{{ isset(@POST.username)?@POST.username:'' }}"> 
            </div>

            <div class="form-group">
                <label>Password</label>
                <input class="form-control" name="password" type="password"> 
            </div>

            <div class="form-group">
                <label>Email</label>
                <input class="form-control" name="email" value="{{ isset(@POST.email)?@POST.email:'' }}"> 
            </div>

        </div>

        <div class="panel-footer">
            <button class="btn btn-success" type="submit">Save</button>
        </div>
    </div>

</form>