
<div class="panel panel-default">
	<div class="panel-heading">
		Admin Users
		<a href="{{ 'user_create' | alias }}" class="btn btn-primary btn-sm pull-right">Create</a>
	</div>
	<div class="panel-body">

		<table class="table table-striped">
			<thead>
				<th>ID</th>
				<th>Username</th>
				<th>Email</th>
				<th>Actions</th>
			</thead>
			<tbody>
			
			<repeat group="{{ @users }}" value="{{ @user }}">
				<tr>
					<td>{{ @user->user_id }}</td>
					<td>{{ @user->username }}</td>
					<td>{{ @user->email }}</td>
					<td>
						<a href="{{ 'user_edit', 'id='.@user->user_id | alias }}" class="btn btn-primary">Edit</a>
						<a href="javascript:void(0);" class="btn btn-danger btn-delete" data-id="{{ @user->user_id }}">Delete</a>
					</td>
				</tr>
			</repeat>

			</tbody>
		</table>
	</div>
</div>



<script>

$(function(){

	$('.btn-delete').on('click', function(e){
		var _this = $(this);
        $.ajax({
        	//A little too hacky for my liking.  Change this when possible.
            url: '{{ 'user_delete', 'id=\'+$(this).data(\'id\')' | alias }},
            type: 'DELETE',
            success: function( data ){
            	//If data.success :]
            	_this.parents('tr').remove();
            }
        });

        return false;
    });

})
    

</script>