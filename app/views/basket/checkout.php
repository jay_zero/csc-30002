<section class="checkout">
<form id="payment-form" action="/checkout" method="post">

	<h1>Checkout</h1>

	<check if="{{ !@user }}">
		<h2>Already have an account?</h2>

		<p>Already have an account with us?  Sign in now to make this process simpler</p>
		<a href="{{ 'login' | alias }}" class="btn">Sign In</a>
	</check>


	<h2>Address Details</h2>

	<!-- If logged in show a drop down select of the addresses-->

	<div class="row">
		<div class="half">
			<h3>Shipping Address</h3>

			<label for="shipping_firstname">Shipping First Name:</label>
			<input type="text" name="shipping_firstname" value="{{ isset(@POST.shipping_firstname)?@POST.shipping_firstname:'' }}" />

			<label for="shipping_lastname">Shipping Surname:</label>
			<input type="text" name="shipping_lastname" value="{{ isset(@POST.shipping_lastname)?@POST.shipping_lastname:'' }}" />

			<label for="shipping_line_1">Shipping Address Line 1:</label>
			<input type="text" name="shipping_line_1" value="{{ isset(@POST.shipping_line_1)?@POST.shipping_line_1:'' }}" />

			<label for="shipping_line_2">Shipping Address Line 2:</label>
			<input type="text" name="shipping_line_2" value="{{ isset(@POST.shipping_line_2)?@POST.shipping_line_2:'' }}" />

			<label for="shipping_city">Shipping City:</label>
			<input type="text" name="shipping_city" value="{{ isset(@POST.shipping_city)?@POST.shipping_city:'' }}" />

			<label for="shipping_postcode">Shipping Postcode:</label>
			<input type="text" name="shipping_postcode" value="{{ isset(@POST.shipping_postcode)?@POST.shipping_postcode:'' }}" />

			<strong>Billing</strong>
			<label><input type="radio" name="use_shipping_as_billing" value="1" checked="checked"/> Use shipping address</label>
			<label><input type="radio" name="use_shipping_as_billing" value="0" /> Use a different address</label>
		</div>

		<div class="half">
		<div id="billing_wrapper" style="display: none">
			<h3>Billing Address</h3>

				<label for="billing_firstname">Billing First Name:</label>
				<input type="text" name="billing_firstname" />

				<label for="billing_lastname">Billing Surname:</label>
				<input type="text" name="billing_lastname" />

				<label for="billing_line_1">Billing Address Line 1:</label>
				<input type="text" name="billing_line_1" />

				<label for="billing_line_2">Billing Address Line 2:</label>
				<input type="text" name="billing_line_2" />

				<label for="billing_city">Billing City:</label>
				<input type="text" name="billing_city" />

				<label for="billing_postcode">Billing Postcode:</label>
				<input type="text" name="billing_postcode" />
			</div>
		</div>
	</div>

	<check if="{{ !@user }}">
	<div class="row">
		<label><input type="checkbox" checked="checked" name="create_account" /> Create an account for me to make ordering easier next time</label>
		<p><em>Don't worry, we never store credit card information and never distribute your address information away from F3Commerce</em></p>

		<label for="customer_email">Email Address:</label>
		<input type="text" name="customer_email" value="{{ isset(@POST.customer_email)?@POST.customer_email:'' }}" />
	</div>
	</check>

	<h2>Order Summary</h2>

	<include href="basket/summary.php" />

	<h2>Payment Details</h2>

	
	<div class="row">
	<div class="half">

		<span class="payment-errors"></span>

		  <div class="form-row">
		    <label>
		      <span>Card Number</span></label>
		      <input type="text" size="20" class="smfield" data-stripe="number"/>

		  </div>

		  <div class="form-row">
		    <label>
		      <span>CVC</span></label>
		      <input type="text" size="4" class="smfield" data-stripe="cvc"/>
		    
		  </div>

		  <div class="form-row">
		    <label>
		      <span>Expiration (MM/YYYY)</span></label>
		      <input type="text" size="2" class="smfield" data-stripe="exp-month"/>
		    <span> / </span>
		    <input type="text" size="4" class="smfield" data-stripe="exp-year"/>
		  </div>
		  </div>
		  <div class="half">
<div class="notice info">
		<h5>Test Card Information</h5>
		<p><strong>Card Number:</strong> 4242 4242 4242 4242</p>
		<p>CV2 can be any 3 digit number and card date just needs to be in the future</p>
	</div>
		  </div>
		 </div>

		  <button type="submit">Submit Payment</button>
	</form>

</section>