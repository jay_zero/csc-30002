<h1>Basket</h1>


<check if="{{ @basket->contents()->items }}">
<true>
	<div class="right">
		<a href="javascript:void(0);" class="btn secondary">Update Basket</a>
		<a href="/checkout" class="btn">Checkout</a>
	</div>

	<table class="basket table">
		<thead>
		<tr>
			<th colspan="2">Item</th>
			<th>Qty</th>
			<th>Price</th>
			<th class="text-right">Total</th>
		</tr>
		</thead>
		<tbody>
			<repeat group="{{ @basket->contents()->items }}" value="{{ @item }}">
				<tr>
					<td width="8%"><img src="{{ @item->product_image }}" /></td>
					<td>{{ @item->product_name }}</td>
					<td><input name="qty" value="{{ @item->quantity }}" size="2"/> <a href="">Update</a> <br /><a href="#"><i class="fa fa-times"></i> Remove</a></td>
					<td>{{ @item->product_price | currency }}</td>
					<td class="text-right">{{ @item->product_price  * @item->quantity | currency }}</td>
				</tr>		
			</repeat>
		</tbody>
	</table>

	<table class="table subtotal">
		<tr>
			<th class="text-right">Sub-Total:</th>
			<td class="text-right"></td>
		</tr>
		<tr>
			<th class="text-right">Tax:</th>
			<td class="text-right"></td>
		</tr>
		<tr>
			<th class="text-right">Total:</th>
			<td class="text-right">{{ @basket->contents()->total }}</td>
		</tr>
	</table>

	<div class="clearfix"></div>

	<div class="right">
		<a href="javascript:void(0);" class="btn secondary">Update Basket</a>
		<a href="/checkout" class="btn">Checkout</a>
	</div>
</true>
<false>
	<div class="notice error">There are currently no items in your basket!</div>
</false>
</check>
