<h1>Order Successful</h1>

<div class="notice success">Your order was placed successfully</div>

<check if="isset(@SESSION.pass)">
	<div class="notice success">We also created an account for you during checkout.  You can login with the email you provided and the password <strong>{{ @SESSION.pass }}</strong></div>
</check>

<h2>Order Summary</h2>

<include href="basket/summary.php" />