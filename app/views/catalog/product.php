<include href="partials/product.php" />

<h2>You might also like</h2>

<section class="product--grid upsell">

	<div class="product--grid_box">
		<img src="http://placehold.it/450x450" />
		<h3 class="name">Lorem Ipsum</h3>
		<div class="price">&pound;19.99</div>
	</div>

	<div class="product--grid_box">
		<img src="http://placehold.it/450x450" />
		<h3 class="name">Lorem Ipsum</h3>
		<div class="price">&pound;19.99</div>
	</div>

	<div class="product--grid_box">
		<img src="http://placehold.it/450x450" />
		<h3 class="name">Lorem Ipsum</h3>
		<div class="price">&pound;19.99 <span>&pound;27.99</span></div>
	</div>

	<div class="product--grid_box">
		<img src="http://placehold.it/450x450" />
		<h3 class="name">Lorem Ipsum</h3>
		<div class="price">&pound;19.99</div>
	</div>

</section>

<include href="modals/basket.php">