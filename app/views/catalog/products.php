<img src="http://placehold.it/1200x200" style="margin-bottom: 40px;" class="hide-mobile"  />

<aside class="sidebar">

	<check if="{{ @children }}">
		<div class="widget">
			<h2 class="widget--title">{{ @category.name }}</h2>
			<div class="widget--body">
				<ul >
					<repeat group="{{ @children }}" value="{{ @child }}">
						<li><a href="/category/{{ @child->slug }}">{{ @child->name }}</a></li>
					</repeat>
				</ul>
			</div>
		</div>
	</check>

	<div class="widget hide-mobile">
		<h2 class="widget--title">Another Block</h2>
		<div class="widget--body">
			<p>Integer facilisis justo leo, ut rutrum mauris congue nec. Quisque suscipit lorem eu pellentesque interdum.</p>
		</div>
	</div>

	<img src="http://placehold.it/450x450&text=upsell" class="hide-mobile" />
</aside>



<div class="primary-content">

	<h1 class="category-title hide-mobile">{{ @category.name }}</h1>
	<check if="{{ @category.description }}">
		<p>{{ @category.description }}</p>
	</check>

	<div class="paging">
		{{ count(@category.products) }} Product(s)
	</div>

	<check if="{{ @category.products }}">
		<true>
		<section class="product--grid">

			<repeat group="{{ @category.products }}" value="{{ @product }}">
				<include href="partials/product-box.php" />
			</repeat>

		</section>
		</true>
		<false>
			<div class="notice error">There are no products in this category just yet!  Please check back soon!</div>
		</false>
	</check>

</div>