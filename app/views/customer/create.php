<h1>Register Account</h1>


<form action="{{ 'register' | alias }}" method="post">

	<!--output any errors-->
	<check if="{{ isset(@SESSION.errors) }}">
        <div class="notice error">
            <repeat group="{{ @SESSION.errors }}" value="@error">
                <p>{{ @error | raw }}</p>
            </repeat>
        </div>
    </check>

	<h3>Your Information</h3>

	<label for="firstname">First Name:</label>
	<input type="text" name="firstname" value="{{ isset(@POST.firstname)?@POST.firstname:'' }}" />

	<label for="lastname">Surname:</label>
	<input type="text" name="lastname" value="{{ isset(@POST.lastname)?@POST.lastname:'' }}" />

	<label for="password">Password:</label>
	<input name="password" type="password" value="{{ isset(@POST.password)?@POST.password:'' }}" />

	<label for="email">Email Address:</label>
	<input type="text" name="email" value="{{ isset(@POST.email)?@POST.email:'' }}" />

	<h3>Your Address</h3>


	<label for="line_1">Address Line 1:</label>
	<input type="text" name="line_1" value="{{ isset(@POST.line_1)?@POST.line_1:'' }}" />

	<label for="line_2">Address Line 2:</label>
	<input type="text" name="line_2" value="{{ isset(@POST.line_2)?@POST.line_2:'' }}" />

	<label for="city">City:</label>
	<input type="text" name="city" value="{{ isset(@POST.city)?@POST.city:'' }}" />

	<label for="postcode">Postcode:</label>
	<input type="text" name="postcode" value="{{ isset(@POST.postcode)?@POST.postcode:'' }}" /> 

	<button type="submit">Create Account</button>
</form>