<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>{{ $title }} - {{ @SITE_NAME }}</title>

    <meta name="stripe-key" content="{{ @STRIPE_PUBLIC_KEY }}" />

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/css/app.css" />

</head>
<body>


    <header class="page-header">
        <div class="top">
            <div class="container">
                <div class="user-menu">
                    <check if="{{ @user }}">
                        <true>Welcome back, {{ @user.firstname }} [ <a href="{{ 'logout' | alias }}">Sign Out</a> ]</true>
                        <false>Welcome Guest <a href="{{ 'login' | alias }}">Sign In</a> or <a href="{{ 'register' | alias }}">Register</a></false>
                    </check>
                </div>
            </div>
        </div>
        <div class="brand">
            <div class="container">
                <div class="logo">
                    <a href="{{ 'home' | alias }}">F3<span>Commerce</span></a>
                </div>
                <div class="search">
                    <form action="/search" method="post">
                        <input name="keywords" type="text" placeholder="Search for anything..." />
                        <button type="submit"><i class="fa fa-search"></i> Search</button>
                    </form>
                </div>
                <div class="mini-basket">
                    {{ @minibasket | raw }}
                </div>
                <!-- <a href="/search" class="search"><i class="fa fa-search"></i></a> -->
            </div>
            <include href="partials/navigation.php" />
        </div>
        
    </header>

    <section class="container">
        {{ @content | raw }}
    </section>

    <div class="container">
        <div class="features-wrap">

            <div class="features-wrap--feature">
                <div class="icon">
                    <i class="fa fa-truck"></i>
                </div>
                <div class="features-wrap--feature_main">
                    <h3>Free Delivery</h3>
                    <p>Get free delivery when you spend &pound;20 on any of our products online.</p>
                </div>
            </div>

            <div class="features-wrap--feature">
                <div class="icon">
                    <i class="fa fa-thumbs-up"></i>
                </div>
                <div class="features-wrap--feature_main">
                    <h3>Customer Satisfaction</h3>
                    <p>We always do our best to please our customers!</p>
                </div>
            </div>

            <div class="features-wrap--feature">
                <div class="icon">
                    <i class="fa fa-lock"></i>
                </div>
                <div class="features-wrap--feature_main">
                    <h3>Secure Payments</h3>
                    <p>Your credit card information never touches our servers.</p>
                </div>
            </div>

        </div>
    </div>


    <footer class="page-footer">

        <div class="feature-push">
            <div class="container">
                <p>Buy from us, because we're amazing.</p>
            </div>
        </div>

        <div class="information">
            <div class="container">
                <div class="block">
                    <h3>Information</h3>
                    <ul>
                        <li><a href="/about">About Us</a></li>
                        <li><a href="/delivery">Delivery Information</a></li>
                        <li><a href="/returns">Returns Policy</a></li>
                        <li><a href="/privacy">Privacy Policy</a></li>
                        <li><a href="/terms">Terms &amp; Conditions</a></li>
                    </li>
                </div>
                <div class="block">
                    <h3>My Account</h3>
                    <ul>
                        <li><a href="/account">My Account</a></li>
                        <li><a href="/account/orders">My Orders</a></li>
                        <li><a href="/account/addresses">My Addresses</a></li>
                    </li>
                </div>
                <div class="block">
                    <h3>Social</h3>
                    <ul>
                        <li><a href="#">Facebook</a></li>
                        <li><a href="#">Twitter</a></li>
                        <li><a href="#">Pinterest</a></li>
                    </li>
                </div>
                <div class="block">
                    <h3>F3<span>Commerce</span></h3>
                    <p>F3Commerce, a simple eCommerce system written using the Fat Free PHP Framework.  The system is designed with flexibility and extensibility in mind and works with strong OOD design principles.  Further controllers can be designed and developed as necessary.</p>
                </div>
            </div>
        </div>

        <div class="copyright">
            <div class="container">
                <p>F3Commerce by <a href="http://www.jamiewatson.me">Jamie Watson</a> &copy; 2015</p>
            </div>
        </div>

    </footer>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="/assets/js/jquery.elevateZoom-3.0.8.min.js"></script>
    <script src="/assets/js/app.min.js"></script>
    <script src="http://localhost:1337/livereload.js"></script>

    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script src="/assets/js/billing.min.js"></script>
    
    <include href="modals/product.php" />
    <include href="modals/basket.php" />

</body>
</html>