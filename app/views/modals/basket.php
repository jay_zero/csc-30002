<div class="modal">
  <input class="modal-state" id="modal-1" type="checkbox" />
  <div class="modal-window">
    <div class="modal-inner">
      <label class="modal-close" for="modal-1"></label>
      <h1>Added to Basket!</h1>
      <p class="modal-intro">Successfully added the tiem to your basket.  What would you like to do next?</p>
      <p><a href="javascript:void(0)" class="close btn secondary">Continue Shopping</a> or <a href="{{ 'basket_index' | alias }}" class="btn primary">Go To Basket</a></p>
    </div>
  </div>
</div>