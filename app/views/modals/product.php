<div class="modal">
  <input class="modal-state" id="product-modal" type="checkbox" />
  <div class="modal-window">
    <div class="modal-inner">
      <label class="modal-close" for="product-modal"></label>
      <div id="modal-content"></div>
    </div>
  </div>
</div>