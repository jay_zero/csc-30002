<img src="http://placehold.it/1100x400" />

<h2 class="feature-title">Featured Products</h2>

<check if="{{ @featured }}">
	<true>
	<section class="product--grid col-4">

		<repeat group="{{ @featured }}" value="{{ @product }}">
			<include href="partials/product-box.php" />
		</repeat>

	</section>
	</true>
	<false>
		<div class="notice error">We've not featured any products just yet!</div>
	</false>
</check>

<h2 class="feature-title">Latest Products</h2>

<check if="{{ @latest }}">
	<true>
	<section class="product--grid col-4">

		<repeat group="{{ @latest }}" value="{{ @product }}">
			<include href="partials/product-box.php" />
		</repeat>

	</section>
	</true>
	<false>
		<div class="notice error">We've not added any products just yet!</div>
	</false>
</check>