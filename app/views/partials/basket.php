<a href="/basket" class="basket">{{ @basket->contents()->count }} items - {{ @basket->contents()->total }}</a>

<div class="contents">
	<table class="mini-cart">
		<repeat group="{{ @basket->contents()->items }}" value="{{ @item }}">
			<tr>
				<td>{{ @item->product_name }}</td>
				<td>x {{ @item->quantity }}</td>
				<td>{{ @item->product_price  * @item->quantity | currency }}</td>
				<td><a href="/basket/remove/{{ @item->product_id }}" class="btn-remove"><i class="fa fa-times"></i></a></td>
			</tr>		
		</repeat>
	</table>

	<check if="{{ @basket->contents()->items }}">
	<true>
		<div class="buttons">
			<a href="/basket" class="btn secondary">View Basket</a>
			<a href="/basket" class="btn">Checkout</a>
		</div>
	</true>
	<false>
		<div class="buttons">
			<div class="notice error">You currently don't have any items in your basket</div>
		</div>
	</false>
	</check>
</div>