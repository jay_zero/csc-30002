<nav class="nav primary">
    <div class="container">
        <ul>
            <li><a href="/">Home</a></li>
            <repeat group="{{ @categories }}" value="{{ @category }}">
                <li><a href="/category/{{ @category->slug }}">{{ @category->name }}</a></li>
            </repeat>
        </ul>
    </div>
</nav>