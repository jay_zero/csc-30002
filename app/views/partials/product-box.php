<div class="product--grid_box">
	<div class="product--detail_basket">
		<form action="/basket/add" method="post">
			<input type="hidden" name="qty" value="1" />
			<input type="hidden" name="product_id" value="{{ @product.product_id }}" />
		<div class="image">
			<a href="{{ 'product', 'slug='.@product.slug | alias }}">
				<img src="/assets/images/products/{{ @product.default_image }}_450x450.jpg" />
				<check if="{{ @product.special_price != 0 }}">
					<span class="badge sale">Sale</span>
				</check>
				<!-- <span class="badge new">New</span> -->
				<span class="image--overlay"></span>
			</a>		
			<div class="image--buttons">
				<a href="{{ 'product', 'slug='.@product.slug | alias }}" class="quick-look"><i class="fa fa-search"></i></a>
				<a href="#" class="btn-basket" data-id="{{ @product.product_id }}"><i class="fa fa-shopping-cart"></i></a>
			</div>

		</div>
		<h3 class="name"><a href="{{ 'product', 'slug='.@product.slug | alias }}">{{ @product.name }}</a></h3>
		<div class="price">
			<check if="{{ @product.special_price != 0 }}">
				<true>
					{{ @product.special_price * (@product.charge_vat?@VAT_RATE:1) | currency }} <span>{{ @product.price * (@product.charge_vat?@VAT_RATE:1) | currency }}</span>
				</true>
				<false>
					{{ @product.price * (@product.charge_vat?@VAT_RATE:1) | currency }}
				</false>
			</check>
		</div>
		</form>
	</div>
</div>