<section class="product">
	<div class="product--images">

		<div class="product--images_large">
			<img src="/assets/images/products/{{ @product.default_image }}_450x450.jpg" alt="Product Image" data-zoom-image="/assets/images/products/{{ @product.default_image }}.jpg"/>
		</div>

		<div class="product--images_thumbs" id="gallery">
		<repeat group="{{ @product.images }}" value="{{ @image }}">
			<a href="#" data-zoom-image"/assets/images/products/{{ @image.file }}.jpg" data-image="/assets/images/products/{{ @image.file }}_450x450.jpg" >
				<img src="/assets/images/products/{{ @image.file }}_100x100.jpg"  alt="{{ @image.caption }}" />
			</a>
		</repeat>
		</div>
	</div>

	<div class="product--detail">
		<h1>{{ @product.name }}</h1>
		<div class="product--detail_meta">
			<dl>
				<dt>SKU:</dt>
				<dd>{{ @product.sku }}</dd>
				<dt>Availability:</dt>
				<dd>{{ @product.stock_level>0?'In Stock':'Out of Stock' }}</dd>
			</dl>
		</div>
		<div class="product--detail_description">
			<p>{{ @product.description }}</p>
		</div>
		<div class="product--detail_price">
			<check if="{{ @product.special_price != 0 }}">
				<true>
					{{ @product.special_price * (@product.charge_vat?@VAT_RATE:1) | currency }} <span>{{ @product.price * (@product.charge_vat?@VAT_RATE:1) | currency }}</span>
				</true>
				<false>
					{{ @product.price * (@product.charge_vat?@VAT_RATE:1) | currency }}
				</false>
			</check>
		</div>

		<check if="@product.stock_level>0">
			<div class="product--detail_basket">
				<form action="/basket/add" method="post">
					<input type="hidden" name="product_id" value="{{ @product.product_id }}" />
					<div class="qty">
						<input name="qty" value="1" />
					</div>
					<button class="btn" type="submit">Add to Basket</button>
				</form>
			</div>
		</check>
		<check if="{{ @product.stock_level > 0 && @product.stock_level < 10 }}">
			<div class="upsell-alert"><strong>Hurry!</strong> only <strong>{{ @product.stock_level }}</strong> remaining!</div>
		</check>
	</div>
</section>