<h1>Search Results for: '{{ htmlentities( @POST.keywords, ENT_QUOTES, 'utf-8') }}'</h1>

<check if="@products">
	<true>
		<section class="product--grid col-4">
			<repeat group="{{ @products }}" value="{{ @product }}">
				<include href="partials/product-box.php" />
			</repeat>
		</section>
	</true>
	<false>
		<div class="notice error">We couldn't find any products with the search term you entered!</div>
	</false>
</check>