<section class="auth">

	<div class="auth--block">
		<div class="auth--block_register">
			<h2>Register for an account</h2>
			<p>Registration is quick and simple.  By registering, you can save your address for faster checkout and view your previous orders</p>
			<a href="{{ 'register' | alias }}" class="btn secondary">Register</a>
		</div>
	</div>

	<div class="auth--block">
		<div class="auth--block_login">
			<h2>Login to your account</h2>

			<form action="" method="post">
				<label for="email">Email Address:</label>
				<input type="text" name="email" />

				<label for="password">Password:</label>
				<input type="password" name="password" />

				<button type="submit">Log In</button>
			</form>
		</div>
	</div>

</section>