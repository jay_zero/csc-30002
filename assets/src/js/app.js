$(function(){
	
	// Plugin for image zoom on product pages.
	$(".product--images_large img").elevateZoom({ gallery:"gallery", zoomType: "inner", cursor:'crosshair'} );


	//Add to basket event
	$('.product--detail_basket .btn, .btn-basket').on('click', function(e){
		addToBasket($(this).parents('form'));
		return false;
	});

	//Show a modal when the add to basket has been clicked
	$('#modal-content').on('click', '.product--detail_basket .btn', function(e){
		addToBasket($(this).parents('form'));
		$('#product-modal').prop('checked', '');
		return false;
	});

	//Product quick look modal
	$('.quick-look').on('click', function(e){

		$.ajax({
			url: $(this).attr('href'),
			success: function( data ){
				$('#modal-content').html( data );
				$('#product-modal').prop('checked', !$(this).prop('checked'));
			}
		})

		return false;
	});

	//Update the mini basket on click of remove item.  Call to /basket/remove
	$('.mini-basket').on('click', '.btn-remove', function(e){
		$.ajax({
			url: $(this).attr('href'),
			dataType: 'json',
			success:function(data){
				$('.mini-basket > a').html( data.count + ' items - ' + data.total );

				$('.mini-basket tr, .mini-basket .buttons a, .mini-basket .notice').remove();

				$.each( data.items, function(i, item){
					$('.mini-basket table').append(
						'<tr>
							<td>'+item.product_name+'</td>
							<td>x'+item.quantity+'</td>
							<td>'+item.price_format+'</td>
							<td><a href="/basket/remove/'+item.product_id+'" class="btn-remove"><i class="fa fa-times"></i></a></td>
						</tr>'
					);
				});

				if( Object.keys(data.items).length > 0 ){
					$('.mini-basket .buttons').append('<a href="/basket" class="btn secondary">View Basket</a> <a href="/checkout" class="btn">Checkout</a>');
				}
			}
		})
		return false;		
	});

	// Single checkout form - Billing / Shipping addresses
	$('input[name=use_shipping_as_billing]').on('change', function(){
		$('#billing_wrapper').fadeToggle();
	})

});


// Function to add an item to the basket: ajax call to /basket/add - on success update the basket.
var addToBasket = function(form){
	$.ajax({
		data: form.serialize(),
		url: '/basket/add',
		type: 'post',
		dataType: 'json',
		success: function( data ){
			if( data.success ){
				$('.mini-basket > a').html( data.count + ' items - ' + data.total );

				$('.mini-basket tr, .mini-basket .buttons a, .mini-basket .notice').remove();

				$.each( data.items, function(i, item){
					$('.mini-basket table').append(
						'<tr>
							<td>'+item.product_name+'</td>
							<td>x'+item.quantity+'</td>
							<td>'+item.price_format+'</td>
							<td><a href="/basket/remove/'+item.product_id+'" class="btn-remove"><i class="fa fa-times"></i></a></td>
						</tr>'
					);
				});

				if( Object.keys(data.items).length > 0 ){
					$('.mini-basket .buttons').append('<a href="/basket" class="btn secondary">View Basket</a> <a href="/checkout" class="btn">Checkout</a>');
				}

				$('html,body').animate({
			        scrollTop: $('.mini-basket').offset().top},
			        'slow');

				$('#modal-1').prop('checked', !$(this).prop('checked'));
			}
		}
	});
}

// Modals
$(function() {
  $("#modal-1").on("change", function() {
    if ($(this).is(":checked")) {
      $("body").addClass("modal-open");
    } else {
      $("body").removeClass("modal-open");
    }
  });

  $(".modal-window").on("click", function() {
    $(".modal-state:checked").prop("checked", false).change();
  });

  $(".modal-inner").on("click", function(e) {
    e.stopPropagation();
  });

  $('.modal .close').on('click', function(){
  	$state = $(this).parents('.modal').find('.modal-state');

  	$state.prop('checked', !$state.prop('checked'));
  	return false;
  })
});
