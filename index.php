<?php

// Kickstart the framework
$f3=require('vendor/f3/base.php');

$f3->set('CACHE', FALSE);
$f3->set('DEBUG',1); // Production should change this value.


if ((float)PCRE_VERSION<7.9)
	trigger_error('PCRE version is out of date');

//PSR-4 Autoload
require('vendor/autoload.php');

//Initialise the IoC Container so that we can bind repositories to interfaces
//This could be used in other instances too.
$container = $f3->set('container', new Illuminate\Container\Container);

//Set up the new SQLite database
$f3->set( 'db', new \DB\SQL('sqlite:ecommerce') );

// Load configuration
$f3->config('config.ini');
$f3->set('basket', new v1l85\Models\Basket);

//Set the ONERROR method in order to output a user friendly error page.
$f3->set('ONERROR',function($f3){
	$controller = new \v1l85\Controllers\PageController($f3);
	$controller->error( $f3 );
});

require_once('app/repositories.php'); // Repository Bindings
require_once('app/helpers.php'); // A couple of helper methods used in the system
require_once('app/routes.php'); // Route definitions

$f3->run();