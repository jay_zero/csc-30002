# F3Commerce

An Ecommerce system built using the [Fat Free framework](http://fatfreeframework.com/).  This system provides a lot of the major features you would expect to see with an online shop:

* Basic Product Catalog ( Simple products only )
* Product Categories
* Order Tracking / Management
* Basic Stock Management ( Stock Levels & what to do on "out of stock" occurence )
* Basket Facility
* Guest Checkout and Customer Accounts
* Customer Address Book Management
* Billing and Payment through Stripe
* Admin User Management


## Installation

Simply clone the repository and place in a suitable web root.  In it's current state, F3Commerce, uses SQLite as the database although changing the database connection to MySQL will not cause any problems.

Ideally, only the public assets + `index.php` should be placed within the public document root whilst the rest of the system above this directory.  In this case, the composer packages `Illuminate\Container` and `Stripe\Stripe-PHP` are supplied in the codebase itself, but ideally they should be requested through the use of composer ( `composer update` ).

## A Word on Structure

F3Commerce has been written in a way in which allows you to extend the project simply.  Using the Illuminate IoC Container and following the Repository Design Pattern allows you to quickly switch out Models if at some stage you want to change the ORM or database type simply.

There should be a high adherence to OOD/OOP principles where possible. Which includes separating out code into relevant classes and, where possible, following appropriate design patterns.  It is this separation of concerns that will ensure flexibility in growth of development.

Everything is PSR4 Autoloaded ( `src/` ) and namespaced under the `v1l85\` namepsace. 

## Going Further

It would be ideal for the system to incorporate such things as the following:

* Shipping / Delivery Methods ( incl. Cash on Delivery, Zonal Pricing, Weight Based etc. )
* Alternative integration with Payment Gateways ( SagePay, PayPal etc. )
* Voucher Codes and Coupons
* Admin Reporting for Management Information
* Switching of "themes" to make this suitable for any online shop
* Installation routine which sets up basic information and installs the relevant database tables


---

Author: [Jamie Watson](http://www.jamiewatson.me) 2015