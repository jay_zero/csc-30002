<?php

namespace v1l85\Controllers\Admin;

use v1l85\Template\Template;

abstract class BaseAdminController{

	protected $db;

	protected $layout = 'admin/layouts/master.php';
	protected $force_login = true;
	
	public function beforeroute( $f3 ){
	
		//"Admintoken" - I separated out the admins vs customers in this case.
		if( $f3->get('SESSION.admintoken') ){
			$f3->set('user', $f3->get('container')->make('v1l85\Repositories\AdminUserRepositoryInterface')->getFirstBy('remember_token', $f3->get('SESSION.admintoken')));
			if( $f3->get('user')->dry() ) $f3->set('SESSION.admintoken', '');
		}else $f3->set('user', []);

		//Ideally I would group each of the routes together and filter those for login
		//Rather than this "hack" to show the login page.
		if( !$f3->get('user') && $this->force_login ){
			$f3->reroute('/admin/login');
		}

		// No HTML ;D Just filter all input and don't allow any HTML at all.
		foreach( $f3->get('POST') as $key => $value ){
			if( is_array( $value ) ){
				foreach( $value as $a => $b )
					$f3->set('POST.'.$key.'['.$a.']', strip_tags($b));
			}else{
				$f3->set('POST.'.$key, strip_tags($value)); 
			}
		}

	}

	public function afterroute( $f3 ){
		echo Template::instance()->render( $this->layout );

		$f3->clear('SESSION.errors');
	}

	public function __construct(){
		$f3 = \Base::instance();
		$f3->set( 'db', new \DB\SQL('sqlite:ecommerce'));
	}

}