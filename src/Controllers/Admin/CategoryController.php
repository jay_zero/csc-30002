<?php

namespace v1l85\Controllers\Admin;
use v1l85\Template\Template;

class CategoryController extends BaseAdminController{

	protected $validator;

	public function __construct( $f3 ){

		parent::__construct( $f3 );
		$this->model = $f3->get('container')->make('v1l85\Repositories\CategoryRepositoryInterface');
		$this->validator = new \v1l85\Validators\CategoryValidator();
	}
	
	public function index( $f3 ){

		$f3->set('categories', $this->model->all() );
		$f3->set('content', Template::instance()->render('admin/category/index.php') );
	}

	public function create( $f3 ){
		$f3->set('categories', $this->model->all() );
		$f3->set('content', Template::instance()->render('admin/category/create.php') );
	}

	public function show( $f3 ){
		//Not needed - in the resource_routes function (helpers.php) should define a list of routes that are not needed from a resource controller
	}

	public function edit( $f3 ){
		$category = $this->model->getById( $f3->get('PARAMS.id') )->copyToAndSave('POST');


		$f3->set('categories', $this->model->all() );
		$f3->set('content', Template::instance()->render('admin/category/edit.php') );
	}

	public function update( $f3 ){
		if( $this->validator->validate( $f3->get('POST') ) ){
			$this->model->save( $f3->get('PARAMS.id') );
			$f3->reroute('@category_index');
		}else{
			$f3->set('SESSION.errors', $this->validator->getErrors() );
			$this->edit( $f3 );
		}
	}

	public function delete( $f3 ){
		$this->model->deleteById( $f3->get('PARAMS.id'));
		exit;
	}

	public function store( $f3 ){
		if( $this->validator->validate( $f3->get('POST') ) ){
			$this->model->create();
		}else{
			$f3->set('SESSION.errors', $this->validator->getErrors() );
			$this->create( $f3 );
		}
	}

}