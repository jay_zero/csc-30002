<?php

namespace v1l85\Controllers\Admin;
use v1l85\Template\Template;

class DashboardController extends BaseAdminController{

	protected $model, $customer_model;

	public function __construct( $f3 ){
		parent::__construct( $f3 );
		$this->model = $f3->get('container')->make('v1l85\Repositories\OrderRepositoryInterface');
		$this->customer_model = $f3->get('container')->make('v1l85\Repositories\CustomerRepositoryInterface');
	}
	
	public function index( $f3 ){
		$f3->set('orderstats', $this->model->getTotalOrders()[0]);
		$f3->set('customers', $this->customer_model->count()[0]);
		$f3->set('sales', $this->model->getOrdersByDate());
		$f3->set('content', Template::instance()->render('admin/dashboard.php') );
	}

}