<?php

namespace v1l85\Controllers\Admin;
use v1l85\Template\Template;

class OrderController extends BaseAdminController{

	public function __construct( $f3 ){
		parent::__construct( $f3 );
		$this->model = $f3->get('container')->make('v1l85\Repositories\OrderRepositoryInterface');
	}
	
	public function index( $f3 ){
		$f3->set('orders', $this->model->sort('date_placed', 'DESC')->all() );
		$f3->set('content', Template::instance()->render('admin/order/index.php') );
	}

	public function create( $f3 ){
		
	}

	public function show( $f3 ){
		$f3->set('order', $this->model->getById( $f3->get('PARAMS.id') )->withOrderLines() );
		$f3->set('content', Template::instance()->render('admin/order/show.php') );
	}

	public function edit( $f3 ){

	}

	public function update( $f3 ){

	}

	public function delete( $f3 ){
		$this->model->deleteById( $f3->get('PARAMS.id'));
		exit;
	}

	public function store( $f3 ){
		$this->model->create();
	}

}