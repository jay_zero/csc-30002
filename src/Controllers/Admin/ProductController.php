<?php

namespace v1l85\Controllers\Admin;
use v1l85\Template\Template;

class ProductController extends BaseAdminController{

	protected $validator;

	public function __construct( $f3 ){
		parent::__construct( $f3 );
		$this->model = $f3->get('container')->make('v1l85\Repositories\ProductRepositoryInterface');
		$this->category_model = $f3->get('container')->make('v1l85\Repositories\CategoryRepositoryInterface');
		$this->validator = new \v1l85\Validators\ProductValidator;
	}
	
	public function index( $f3 ){
		$f3->set('products', $this->model->all() );
		$f3->set('content', Template::instance()->render('admin/product/index.php') );
	}

	public function create( $f3 ){
		$f3->set('categories', $this->category_model->all() );
		$f3->set('content', Template::instance()->render('admin/product/create.php') );
	}

	public function show( $f3 ){
		
	}

	public function edit( $f3 ){
		$product = $this->model->getById( $f3->get('PARAMS.id') )->withCategories()->withImages();
		$product->copyToAndSave('POST');

		$f3->mset([
			'category_list'		=>	$product->category_list,
			'categories'		=>	$this->category_model->all(),
			'images'			=>	$product->images,
			'default_image'		=>	$product->default_image
		]);

		$f3->set('content', Template::instance()->render('admin/product/edit.php') );
	}

	public function update( $f3 ){
		if( $this->validator->validate( $f3->get('POST') ) ){
			$this->model->save( $f3->get('PARAMS.id') );
			$f3->reroute('@product_index');
		}else{
			$f3->set('SESSION.errors', $this->validator->getErrors() );
			$this->edit( $f3 );
		}
	}

	public function delete( $f3 ){
		$this->model->deleteById( $f3->get('PARAMS.id'));
		exit;
	}

	public function store( $f3 ){
		if( $this->validator->validate( $f3->get('POST') ) ){
			$this->model->create();
			$f3->reroute( '@product_index' );
		}else{
			$f3->set('SESSION.errors', $this->validator->getErrors() );
			$this->create( $f3 );
		}
	}

}