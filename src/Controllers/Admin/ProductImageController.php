<?php

namespace v1l85\Controllers\Admin;
use v1l85\Template\Template;

class ProductImageController extends BaseAdminController{

	protected $validator;

	public function __construct( $f3 ){
		parent::__construct( $f3 );
		$this->model = $f3->get('container')->make('v1l85\Repositories\ProductImageRepositoryInterface');
	}

	public function delete( $f3 ){
		$this->model->deleteById( $f3->get('PARAMS.id'));
		exit;
	}

}