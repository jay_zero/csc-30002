<?php

namespace v1l85\Controllers\Admin;
use v1l85\Template\Template;

class SessionController extends BaseAdminController{

	protected $model;
	protected $force_login = false;
	protected $layout = 'admin/layouts/full.php';
	
	public function __construct( $f3 ){
		parent::__construct($f3);
		$container = $f3->get('container');
		$this->model = $container->make('\v1l85\Repositories\AdminUserRepositoryInterface');

	}

	public function index( $f3 ){
		$this->title = "Sign In";
		$f3->set('content', Template::instance()->render('admin/session/index.php') );
	}

	public function create( $f3 ){
		
		/*	
			// A better way using Blowfish Encryption on the passwords instead of a rudimentary MD5

			$user = $this->model->getFirstBy('username', $f3->get('POST.email') );

			if( \Bcrypt::instance()->verify( $f3->get('POST.password'), $user->password ) ){
				// Do login stuff
				dd('Login Successful');
			}else{
				dd('Username or password incorrect.');
			}
		*/		 

		$auth = new \Auth( new \DB\SQL\Mapper($f3->get('db'), 'admin_users'), array('id' => 'email', 'pw' => 'password'));

		//Login using an md5 password, not great but at least it's not plain text - see above for an improved hash
		if( $auth->login($f3->get('POST.email'),md5($f3->get('POST.password'))) ){

			$user = $this->model->getFirstBy('email', $f3->get('POST.email') );

			$token = md5( uniqid( rand(), true));
			$f3->set('SESSION.admintoken', $token);

			$user->remember_token = $token;
			$user->save();

			$f3->reroute('/admin');
		}else{ $f3->reroute('/admin/login'); }


	}

	public function delete( $f3 ){
		if( $f3->get('user') ){
			$f3->get('user')->remember_token = '';
			$f3->get('user')->save();
		}

		$f3->set('SESSION.admintoken', '');
		$f3->reroute('/admin/login');
	}

}