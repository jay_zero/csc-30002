<?php

namespace v1l85\Controllers\Api;

abstract class BaseApiController{
	
	protected $repository;

	public function __construct( $repository = null ){
		$this->repository = $repository;
	}

	public function index(){
		$raw = $this->repository->all();
		
		$records = [];
		foreach( $raw as $row )
			$records[] = $row->cast();

		$this->respondWithJSON( $records );
	}

	public function show( $f3 ){
		$record = $this->repository->getById($f3->get('PARAMS.id'))->cast();	
		$this->respondWithJSON( $record );
	}

	protected function respondWithJSON( $arr = [] ){
		echo json_encode( $arr );
	}

}