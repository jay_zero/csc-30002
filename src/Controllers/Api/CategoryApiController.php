<?php

namespace v1l85\Controllers\Api;

class CategoryApiController extends BaseApiController{
	

	public function __construct(){
		parent::__construct( \Base::instance()->get('container')->make('v1l85\Repositories\CategoryRepositoryInterface') );
	}

	//Override the parent show method ... categories should come with products
	public function show( $f3 ){

		//Just a little bit hacky for my liking but F3's cast function destroys the "relationships" (not really relationships!)
		//that I added.  So in essence, just make sure we save the products that we've just pulled from the repository
		//and output them to the json.
		$record = $this->repository->getById($f3->get('PARAMS.id'))->withProducts();
		$products = $record->products;

		$category = $record->cast();
		$category['products'] = $products;

		$this->respondWithJSON( $category );
	}

}