<?php

namespace v1l85\Controllers\Api;

class OrderApiController extends BaseApiController{

	public function __construct(){
		parent::__construct( \Base::instance()->get('container')->make('v1l85\Repositories\OrderRepositoryInterface') );
	}

	public function store(){


		$shipping_address = [
			'shipping_firstname'	=>	$f3->get('POST.shipping_firstname'),
			'shipping_lastname'		=>	$f3->get('POST.shipping_lastname'),
			'shipping_line_1'		=>	$f3->get('POST.shipping_line_1'),
			'shipping_line_2'		=>	$f3->get('POST.shipping_line_2'),
			'shipping_postcode'		=>	$f3->get('POST.shipping_postcode'),
			'shipping_city'			=>	$f3->get('POST.shipping_city')
		];

		if( $f3->get('POST.use_shipping_as_billing') != 1 ){
			$billing_address = [
				'billing_firstname'		=>	$f3->get('POST.billing_firstname'),
				'billing_lastname'		=>	$f3->get('POST.billing_lastname'),
				'billing_line_1'		=>	$f3->get('POST.billing_line_1'),
				'billing_line_2'		=>	$f3->get('POST.billing_line_2'),
				'billing_postcode'		=>	$f3->get('POST.billing_postcode'),
				'billing_city'			=>	$f3->get('POST.billing_city')
			];
		}else{
			$billing_address = [
				'billing_firstname'		=>	$shipping_address['shipping_firstname'],
				'billing_lastname'		=>	$shipping_address['shipping_lastname'],
				'billing_line_1'		=>	$shipping_address['shipping_line_1'],
				'billing_line_2'		=>	$shipping_address['shipping_line_2'],
				'billing_postcode'		=>	$shipping_address['shipping_postcode'],
				'billing_city'			=>	$shipping_address['shipping_city']
			];
		}

		//Save the order
		$f3->set('ORDER', array_merge([
			'order_total'		=>	(int)$total,
			'stripe_token'		=>	null,  /// Coming from the API we don't use the Stripe Billing here
			'vat_rate'			=>	$f3->get('VAT_RATE'),
			'order_status_id'	=> 	0,
			'date_placed'		=>	time(),
			'last_update'		=>	time(),
			'customer_id'		=>	0 // Don't associate a customer - this would be managed on the third party calling the api route
		], $shipping_address, $billing_address));
		$order = $this->order_model->create( 'ORDER' );

		//Save basket item.
		foreach( $this->basket->contents()->items as $item ){
			$product = $this->product_model->getById( $item->product_id );
			$product->stock_level-=$item->quantity;
			$product->save();
			$f3->set('ORDER_LINE', [
				'product_id'		=>	$product->product_id,
				'product_name'		=>	$product->name,
				'price'				=>	$product->special_price?:$product->price,
				'quantity'			=>	$item->quantity,
				'charge_vat'		=>	$product->charge_vat,
				'order_id'			=>	$order->order_id
			]);

			$this->order_line_model->create( 'ORDER_LINE' );
		}


	}

}