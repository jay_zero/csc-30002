<?php

namespace v1l85\Controllers\Api;

class ProductApiController extends BaseApiController{
	

	public function __construct(){
		parent::__construct( \Base::instance()->get('container')->make('v1l85\Repositories\ProductRepositoryInterface') );
	}

	//Override the parent show method ... categories should come with products
	public function show( $f3 ){

		//Same as with categories.  Except we need to pull the product images
		//In fact.  Thinking about it... data transformers might be useful here... or some kind of "service"
		//that does this kind of work for me
		$record = $this->repository->getById($f3->get('PARAMS.id'))->withImages();


		//I shouldn't be doing this in the controller at all! >.<
		$images = $record->images;
		$default = $record->default_image;

		$product = $record->cast();

		$product['images'] = [];
		foreach( $images as $image )
			$product['images'][] = $image->cast();

		$product['default_image'] = $default;
		// End stuff to be moved to a more logical place

		$this->respondWithJSON( $product );
	}

}