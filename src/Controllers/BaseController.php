<?php
namespace v1l85\Controllers;

use v1l85\Template\Template;

abstract class BaseController{
	
	protected $db;

	protected $layout = 'layouts/master.php'; // Default layout.  Concrete controllers may override this.
	protected $title = '';

	public function __construct(){

		$f3 = \Base::instance();

		// View Composer kind of stuff really - maybe I'll extend the templating system in F3 if I have time.
		// Basically make the categories and the "mini basket" available for each request.
		// Likely overkill for ajax based requests that will never output this
		$category_model = $f3->get('container')->make('v1l85\Repositories\CategoryRepositoryInterface');
		$f3->set('categories', $category_model->getManyBy('parent_id', 0) );
		$f3->set('minibasket', Template::instance()->render('partials/basket.php') );

		//Check User Login
		//Does the user have a session token?  If they do, check that against the databse otherwise the user is not assumed to be logged in
		if( $f3->get('SESSION.token') ){
			$f3->set('user', $f3->get('container')->make('v1l85\Repositories\CustomerRepositoryInterface')->getFirstBy('remember_token', $f3->get('SESSION.token')));
			if( !$f3->get('user') ) $f3->set('SESSION.token', '');
		}else $f3->set('user', []);
		
	}

	public function afterroute( $f3 ){
		
		$f3->set('title', $this->title );
		echo Template::instance()->render( $this->layout );

		//Clear SESSION flashdata (ideally this would be all flash data not just errors)
		//Would be nice to have a facility for Session::Flash() or something similar .. but for now just use the F3 Hive
		$f3->clear('SESSION.errors');
	}

}