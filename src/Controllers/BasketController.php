<?php

namespace v1l85\Controllers;
use v1l85\Template\Template;

class BasketController extends BaseController{

	protected $basket;

	public function __construct($f3){
		parent::__construct($f3);
		$this->basket = $f3->get('basket');

		$this->product_model = $f3->get('container')->make('\v1l85\Repositories\ProductRepositoryInterface');
	}

	public function show( $f3 ){
		$this->title = "Basket";
		$f3->set('content', Template::instance()->render('basket/index.php'));

	}

	public function create( $f3 ){

		//No product ID supplied, don't bother continuing		
		if( !$f3->get('REQUEST.product_id') || $f3->get('REQUEST.qty') == 0 ){
			echo json_encode( array_merge(['success' => false, 'message' => 'The appropriate product information was not supplied.'], (array) $this->basket->contents()) );
			exit; 
		}

		$product = $this->product_model->getById( $f3->get('REQUEST.product_id') )->withDefaultImage();

		if( !$product ){
			echo json_encode( array_merge(['success' => false, 'message' => 'The product identifier you supplied was invalid.'], (array) $this->basket->contents()) );
			exit; 
		}

		$this->basket->load('product_id', $product->product_id );
		$qty = $this->basket->exists('quantity')?$this->basket->get('quantity'):0;


		$price = ($product->special_price?:$product->price) * ($product->charge_vat?$f3->get('VAT_RATE'):1.00);

		$this->basket->set('product_id', $product->product_id);
		$this->basket->set('product_name', $product->name);
		$this->basket->set('product_slug', $product->slug);
		$this->basket->set('product_price', $price);
		$this->basket->set('product_image', product_image_url( $product->default_image, '100x100' ));
		$this->basket->set('quantity', $qty + $f3->get('REQUEST.qty'));
		
		if( $this->basket->save() ){
			echo json_encode( array_merge(['success' => true, 'message' => 'Successfully added item to your basket!'], (array) $this->basket->contents()) );
		}else{
			echo json_encode( array_merge(['success' => false, 'message' => 'Could not add item to basket, please try again'], (array) $this->basket->contents()) );
		}

		$this->basket->reset();
		exit;

	}

	public function remove( $f3 ){
		$this->basket->erase('product_id', $f3->get('PARAMS.id'));
		echo json_encode( array_merge(['success' => true, 'message' => ''], (array) $this->basket->contents()) );
		exit;
	}

	public function index( $f3 ){
		echo json_encode( $f3->get('basket')->contents() );
		exit;

	}

	public function delete(){
		$this->basket->drop();
		exit;
	}

}