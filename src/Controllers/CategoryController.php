<?php

namespace v1l85\Controllers;

use v1l85\Models\Product as Product;
use v1l85\Template\Template;

class CategoryController extends BaseController{

	public function __construct( $f3 ){
		parent::__construct( $f3 );
		$this->model = $f3->get('container')->make('v1l85\Repositories\CategoryRepositoryInterface');
	}
	

	public function show( $f3, $args ){

		$category = $this->model->getFirstBy('slug', $f3->get('PARAMS.slug') )->withProducts();

		if($category->dry()) $f3->error(404);
		$this->title = $category->name;

		$f3->set('category', $category );
		$f3->set('children', $this->model->getManyBy('parent_id', $category->category_id));
		$f3->set('content', Template::instance()->render('catalog/products.php'));

	}


}