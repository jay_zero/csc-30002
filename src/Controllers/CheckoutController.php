<?php

namespace v1l85\Controllers;

use Stripe\Stripe as Stripe;
use Stripe\Charge as Stripe_Charge;
use Stripe\Error\Card as Stripe_CardError;
use v1l85\Template\Template;

class CheckoutController extends BaseController{
	
	protected $basket;

	public function __construct( $f3 ){
		parent::__construct( $f3 );
		$this->basket = $f3->get('basket');

		// Register the required repository interfaces
		$this->product_model = $f3->get('container')->make('v1l85\Repositories\ProductRepositoryInterface');
		$this->order_model = $f3->get('container')->make('v1l85\Repositories\OrderRepositoryInterface');
		$this->order_line_model = $f3->get('container')->make('v1l85\Repositories\OrderLineRepositoryInterface');
		$this->customer_model = $f3->get('container')->make('v1l85\Repositories\CustomerRepositoryInterface');
		$this->customer_address_model = $f3->get('container')->make('v1l85\Repositories\CustomerAddressRepositoryInterface');
	}

	public function index( $f3 ){

		$this->title = "Checkout";
		if( $f3->get('user') ){
			$address = $this->customer_address_model->getFirstBy('customer_id', $f3->get('user')->customer_id )->cast();
			foreach( $address as $index => $value )
				$address['POST.shipping_' . $index] = $value;

			$address['POST.shipping_firstname'] = $f3->get('user')->firstname;
			$address['POST.shipping_lastname'] = $f3->get('user')->lastname;
			$f3->mset( $address );
		}

		$f3->set('content', Template::instance()->render('basket/checkout.php'));
	} 

	public function store( $f3 ){

		// dd( $_POST );
		
		//Needs to validate the customer info input + on create new account check account doesn't exist etc.
		//Check customer details

		// Were we provided with an address id?  If not, pull from the form? (maybe) 

		$shipping_address = [
			'shipping_firstname'	=>	$f3->get('POST.shipping_firstname'),
			'shipping_lastname'		=>	$f3->get('POST.shipping_lastname'),
			'shipping_line_1'		=>	$f3->get('POST.shipping_line_1'),
			'shipping_line_2'		=>	$f3->get('POST.shipping_line_2'),
			'shipping_postcode'		=>	$f3->get('POST.shipping_postcode'),
			'shipping_city'			=>	$f3->get('POST.shipping_city')
		];

		if( $f3->get('POST.use_shipping_as_billing') != 1 ){
			$billing_address = [
				'billing_firstname'		=>	$f3->get('POST.billing_firstname'),
				'billing_lastname'		=>	$f3->get('POST.billing_lastname'),
				'billing_line_1'		=>	$f3->get('POST.billing_line_1'),
				'billing_line_2'		=>	$f3->get('POST.billing_line_2'),
				'billing_postcode'		=>	$f3->get('POST.billing_postcode'),
				'billing_city'			=>	$f3->get('POST.billing_city')
			];
		}else{
			$billing_address = [
				'billing_firstname'		=>	$shipping_address['shipping_firstname'],
				'billing_lastname'		=>	$shipping_address['shipping_lastname'],
				'billing_line_1'		=>	$shipping_address['shipping_line_1'],
				'billing_line_2'		=>	$shipping_address['shipping_line_2'],
				'billing_postcode'		=>	$shipping_address['shipping_postcode'],
				'billing_city'			=>	$shipping_address['shipping_city']
			];
		}


		// If create_account AND we're not already logged in AND the desired username / email don't already exist.
		if( $f3->get('POST.create_account') ){
			$f3->set('SESSION.pass', \Base::instance()->hash(uniqid(rand(),true)));
			$f3->set('CUSTOMER', [
				'password'		=>	$f3->get('SESSION.pass'),
				'email'			=>	$f3->get('POST.customer_email'),
				'firstname'		=>	$f3->get('POST.shipping_firstname'),
				'lastname'		=>	$f3->get('POST.shipping_lastname')
			]);
			$customer = $this->customer_model->create( 'CUSTOMER' );

			//Save Address(es) to customer_addresses
			foreach( $shipping_address as $field => $value ){
				$address[ str_replace('shipping_','',$field) ] = $value;
			}
			$address['customer_id'] = $customer->customer_id;
			$f3->set('ADDRESS', $address );
			$this->customer_address_model->create( 'ADDRESS' );

			if( $f3->get('POST.use_shipping_as_billing') != 1 ){
				foreach( $billing_address as $field => $value ){
					$address[ str_replace('billing_','',$field) ] = $value;
				}
				$address['customer_id'] = $customer->customer_id;
				$f3->set('ADDRESS', $address );
				$this->customer_address_model->create( 'ADDRESS' );
			}
		}else $customer_id = $f3->get('user')->customer_id; // Otherwise customer id = 0 or current user id


		//Basket Totals
		$total = 0;
		foreach( $this->basket->contents()->items as $item ){
			$product = $this->product_model->getById( $item->product_id );
			$total += ($product->special_price?:$product->price) * ($product->charge_vat?1.2:1);
		}

		//Do Stripe work
		Stripe::setApiKey($f3->get('STRIPE_SECRET_KEY'));

		// // Get the credit card details submitted by the form
		$token = $f3->get('POST.stripeToken');

		// Create the charge on Stripe's servers - this will charge the user's card
		try {
			$charge = Stripe_Charge::create(array(
			  "amount" => (int)$total, 
			  "currency" => "gbp",
			  "card" => $token,
			  "description" => "F3Commerce Purchase")
			);
		} catch(Exception $e) {
		  // The card has been declined
		  dd( $e );
		}

		//Create a new Order (CreateNewOrderService)
		$f3->set('ORDER', array_merge([
			'order_total'		=>	(int)$total,
			'stripe_token'		=>	$token,
			'vat_rate'			=>	$f3->get('VAT_RATE'),
			'order_status_id'	=> 	0,
			'customer_id'		=>	0,
			'date_placed'		=>	time(),
			'last_update'		=>	time(),
			'customer_id'		=>	$customer->customer_id
		], $shipping_address, $billing_address));
		$order = $this->order_model->create( 'ORDER' );

		foreach( $this->basket->contents()->items as $item ){
			$product = $this->product_model->getById( $item->product_id );
			$product->stock_level-=$item->quantity;
			$product->save();
			$f3->set('ORDER_LINE', [
				'product_id'		=>	$product->product_id,
				'product_name'		=>	$product->name,
				'price'				=>	$product->special_price?:$product->price,
				'quantity'			=>	$item->quantity,
				'charge_vat'		=>	$product->charge_vat,
				'order_id'			=>	$order->order_id
			]);

			$this->order_line_model->create( 'ORDER_LINE' );
		}

		//Redirect to /checkout/success with ORDER REFERENCE & if applicable new account information
		$f3->reroute('@checkout_success');

	}

	public function show( $f3 ){
		//Show successful order
		//Display basket contents and "destroy"
		$f3->set('content', Template::instance()->render('basket/success.php'));
		$f3->get('basket')->drop();
		$f3->clear('SESSION.pass');//Wouldn't normally do this, the password would either be selected or emailed to the user.
	}

}