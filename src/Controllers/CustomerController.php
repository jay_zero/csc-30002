<?php

namespace v1l85\Controllers;
use v1l85\Template\Template;

class CustomerController extends BaseController{

	public function __construct( $f3 ){

		parent::__construct( $f3 );
		$this->model = $f3->get('container')->make('v1l85\Repositories\CustomerRepositoryInterface');
		$this->address_model = $f3->get('container')->make('v1l85\Repositories\CustomerAddressRepositoryInterface');

		$this->validator = new \v1l85\Validators\CustomerValidator;
		$this->address_validator = new \v1l85\Validators\AddressValidator;
	}
	
	public function create( $f3 ){
		$f3->set('content', Template::instance()->render('customer/create.php') );
	}

	public function store( $f3 ){
		if( $this->validator->validate( $f3->get('POST') ) && $this->address_validator->validate( $f3->get('POST') ) ){
			$customer = $this->model->create();
			$f3->set('POST.customer_id', $customer->customer_id );
			$this->address_model->create();
			$f3->reroute('@login');
		}else{
			$f3->set('SESSION.errors', array_merge( $this->validator->getErrors(), $this->address_validator->getErrors() ) );
			$this->create( $f3 );
		}
	}

}