<?php

namespace v1l85\Controllers;

use v1l85\Repositories\OrderRepositoryInterface;
use v1l85\Template\Template;

class PageController extends BaseController{

	protected $model, $product_model;
	
	public function __construct( $f3 ){

		parent::__construct($f3);

		//Grab the IoC Container and make the relevant repositories
		$container = $f3->get('container');
		$this->model = $container->make('\v1l85\Repositories\PageRepositoryInterface');
		$this->product_model = $container->make('\v1l85\Repositories\ProductRepositoryInterface');

	}

	public function home( $f3 ){

		$this->title = 'Home';
		//Move this kind of processing to the model / repository
		$featured = $this->product_model->limit(4)->getManyBy('featured', 1);
		foreach( $featured as $product )
			$product->withDefaultImage();

		$latest = $this->product_model->limit(4)->sort('product_id', 'DESC')->all();
		foreach( $latest as $product )
			$product->withDefaultImage();

		$f3->set('featured',  $featured );
		$f3->set('latest',  $latest );
		$f3->set('content', Template::instance()->render('pages/home.php') );
	}

	public function page( $f3 ){

		$page = $this->model->getFirstBy('slug', $f3->get('PARAMS.slug') );
		if($page->dry()) $f3->error(404);

		$this->title = $page->name;
		$f3->set('content', \Markdown::instance()->convert($page->content) );
	}

	public function error( $f3 ){

		//Show an explicit 404 error message or generic one for everything else
		// dd( $f3->get('ERROR.status'));
		if( $f3->get('ERROR.code') == 404 ){
			$f3->set('title', 'Page Not Found');
			$f3->set('content', Template::instance()->render('pages/404.php') );
		}else{
			$f3->set('title', $f3->get('ERROR.status'));
			$f3->set('content', Template::instance()->render('pages/error.php') );
		}

		echo Template::instance()->render('layouts/master.php');
	}

}