<?php

namespace v1l85\Controllers;

use v1l85\Template\Template;

class ProductController extends BaseController{

	public function __construct( $f3 ){
		parent::__construct( $f3 );
		$this->model = $f3->get('container')->make('v1l85\Repositories\ProductRepositoryInterface');
		$this->category_model = $f3->get('container')->make('v1l85\Repositories\CategoryRepositoryInterface');
	}
	

	public function show( $f3 ){
		$product = $this->model->getFirstBy('slug', $f3->get('PARAMS.slug') )->withImages();

		if( $product->dry() ) $f3->error(404);

		$this->title = $product->name;
		
		$product->views++;
		$product->save();
		
		$f3->set('product', $product);
		$f3->set('content', Template::Instance()->render('catalog/product.php'));
	}

	public function showAjax( $f3 ){
		$product = $this->model->getFirstBy('slug', $f3->get('PARAMS.slug') )->withImages();

		$product->views++;
		$product->save();
		
		$f3->set('product', $product);
		echo Template::Instance()->render('partials/product.php');
		exit;
	}

}