<?php

namespace v1l85\Controllers;

use v1l85\Template\Template;

class SearchController extends BaseController{

	public function __construct( $f3 ){
		parent::__construct( $f3 );
		$this->model = $f3->get('container')->make('v1l85\Repositories\ProductRepositoryInterface');
	}
	

	public function index( $f3 ){
		$products = $this->model->searchManyBy('name', $f3->get('POST.keywords') );

		foreach( $products as $product )
			$product->withDefaultImage();

		$this->title = "Search Results";
		
		$f3->set('products', $products);
		$f3->set('content', Template::Instance()->render('search/index.php'));
	}


}