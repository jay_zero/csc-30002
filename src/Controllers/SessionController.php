<?php

namespace v1l85\Controllers;
use v1l85\Template\Template;

class SessionController extends BaseController{

	protected $model;
	
	public function __construct( $f3 ){
		parent::__construct($f3);
		$container = $f3->get('container');
		$this->model = $container->make('\v1l85\Repositories\CustomerRepositoryInterface');

	}

	public function index( $f3 ){
		$this->title = "Sign In";
		$f3->set('content', Template::instance()->render('session/index.php') );
	}

	public function create( $f3 ){

		$auth = new \Auth( new \DB\SQL\Mapper($f3->get('db'), 'customers'), array('id' => 'email', 'pw' => 'password'));

		if( $auth->login($f3->get('POST.email'),md5($f3->get('POST.password'))) ){

			$user = $this->model->getFirstBy('email', $f3->get('POST.email') );

			$token = md5( uniqid( rand(), true));
			$f3->set('SESSION.token', $token);

			$user->remember_token = $token;
			$user->save();

			$f3->reroute('/');
		}else{ $f3->reroute('/login'); }


	}

	public function delete( $f3 ){
		if( $f3->get('user') ){
			$f3->get('user')->remember_token = '';
			$f3->get('user')->save();
		}

		$f3->set('SESSION.token', '');
		$f3->reroute('/');
	}

}