<?php


namespace v1l85\Models;

class AdminUser extends F3BaseModel{
	
	public $primary_key = "user_id";

	protected $table = "admin_users";

	public $fillable = ['username', 'password', 'email'];

}
