<?php

namespace v1l85\Models;

class Basket extends \Basket{
	
	protected $fields = ['product_id', 'product_name', 'product_price', 'quantity', 'product_image'];
	protected $check = array();

	/*
		@ Just additional helper method to output the basket
	 */
	public function contents(){

		// Specialised basket.  We don't want ANY records which aren't fully set
		// so remove and delete.  Otherwise output the contents to an array.

		$items = array();
		$total = 0;
		$i = 0;
		$qty = 0;

		foreach( $this->find() as $item ){
			$i++;
			$items[$i] = new \stdClass;
			foreach( $this->fields as $field ){
				if( empty( $item->{$field} ) ){
					unset( $items[$i] );
				}else{
					$items[$i]->{$field} = $item->{$field};
					if( $field == 'product_price' ){
						$total+= $item->product_price*$item->quantity;
						$items[$i]->price_format = '&pound;' . number_format(((int)($item->product_price*$item->quantity) / 100),2);
					}
					if( $field == 'quantity' ){
						$qty += $item->quantity;
					}
				}
			}
		}

		$basket = new \stdClass;
		$basket->total =  '&pound;' .number_format(((int)$total / 100),2);
		$basket->count = $qty;
		$basket->items = $items;
	
		return $basket;

	}
	

}