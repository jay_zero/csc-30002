<?php


namespace v1l85\Models;

class Category extends F3BaseModel{
	
	public $primary_key = "category_id";

	protected $table = "categories";
	public $products = array();

	public $fillable = ['name', 'description', 'parent_id', 'slug'];


	/**
	 * Allows the facility to pull out categories with all of their associated products
	 * 
	 * @return v1l85\Models\Category
	 */
	public function withProducts(){
		
		$records = \Base::instance()->get('db')->exec('
				SELECT p.*, pi.file as default_image
					FROM products p
					INNER JOIN products_categories pc USING(product_id)
					INNER JOIN categories c USING(category_id)
					INNER JOIN product_images pi ON default_image_id = pi.product_image_id
					WHERE category_id=?', $this->category_id );

		foreach( $records as $row )
			$this->products[] = $row;

		return $this;
	}

}
