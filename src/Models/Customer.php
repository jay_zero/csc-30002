<?php


namespace v1l85\Models;

class Customer extends F3BaseModel{
	
	public $primary_key = "customer_id";

	protected $table = "customers";

	public $fillable = ['password', 'email', 'firstname', 'lastname', 'title'];

}
