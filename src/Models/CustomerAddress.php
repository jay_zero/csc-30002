<?php


namespace v1l85\Models;

class CustomerAddress extends F3BaseModel{
	
	public $primary_key = "address_id";

	protected $table = "customer_addresses";

	public $fillable = ['line_1', 'line_2', 'city', 'postcode', 'country', 'customer_id'];

}
