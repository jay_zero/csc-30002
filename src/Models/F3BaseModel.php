<?php

namespace v1l85\Models;

abstract class F3BaseModel extends \DB\SQL\Mapper{
	
	public $primary_key = 'id';

	protected $table;

	public $fillable = [];

	public function __construct(){
		$f3 = \Base::instance();
		parent::__construct( $f3->get('db'), $this->table );
	}

	//Just a quick function to save data to a hive variable without overwriting it if it exists
	public function copyToAndSave( $var ){

		$this->copyTo('DATA_ITEM');

		foreach( \Base::instance()->get('DATA_ITEM') as $field => $value )
			\Base::instance()->set($var.'.'.$field, \Base::instance()->get($var.'.'.$field)!==null?$_POST[$field]:$value );

		\Base::instance()->clear('DATA_ITEM');
	}

}