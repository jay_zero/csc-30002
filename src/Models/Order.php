<?php


namespace v1l85\Models;

class Order extends F3BaseModel{
	
	public $primary_key = "order_id";

	protected $table = "orders";
	public $lines = array();

	public $fillable = [
			'date_placed', 'order_status_id', 'order_total', 'customer_id','stripe_token', 'vat_rate', 
			'billing_line_1','billing_line_2', 'billing_postcode', 'billing_city', 'billing_country',
			'shipping_line_1','shipping_line_2', 'shipping_postcode', 'shipping_city', 'shipping_country',
			'billing_firstname', 'billing_lastname', 'shipping_firstname', 'shipping_lastname'
		];

	public function withOrderLines(){
		
		//I know this is not a great 'relationship' and that a join would be better than 2 queries to the database
		//But this way allows me to keep using the structure of the ORM
		$records = \Base::instance()->get('db')->exec('
				SELECT o.*
					FROM order_lines o
					WHERE order_id=?', $this->order_id );

		foreach( $records as $row )
			$this->lines[] = $row;

		return $this;
	}

}
