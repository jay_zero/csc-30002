<?php


namespace v1l85\Models;

class OrderLine extends F3BaseModel{
	
	public $primary_key = "order_line_id";

	protected $table = "order_lines";

	public $fillable = ['product_name', 'price', 'product_id', 'quantity', 'charge_vat', 'order_id'];

}
