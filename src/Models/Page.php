<?php


namespace v1l85\Models;

class Page extends F3BaseModel{
	
	public $primary_key = "page_id";

	protected $table = "pages";
	public $fillable = ['name', 'content', 'slug'];


}
