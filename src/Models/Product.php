<?php


namespace v1l85\Models;

class Product extends F3BaseModel{
	
	protected $table = 'products';

	public $primary_key = 'product_id';
	public $images = array(); //Have to do this because of the mapper object
	public $categories = array(); //Have to do this because of the mapper object
	public $category_list = array(); //Have to do this because of the mapper object
	public $default_image = 'no-image.jpg'; //Have to do this because of the mapper object

	public $fillable = [
		'name', 'description', 'price', 'special_price', 'charge_vat', 'default_image_id', 
		'stock_level', 'out_of_stock_status', 'available_from', 'visible', 'slug', 'featured', 'sku'
	];

	public function __construct(){
		parent::__construct();
	}

	public function findInCategory( $id ){

		return $this->db->exec('
			SELECT p.* FROM products p
				INNER JOIN products_categories pc USING(product_id)
				INNER JOIN categories c USING(category_id)
				WHERE category_id=?
		', $id);

	}

	public function withCategories(){
		
		$categories = $this->db->exec('
			SELECT * FROM products_categories pc
			INNER JOIN categories c USING(category_id) 
			WHERE product_id=?', $this->product_id );

		foreach( $categories as $category ){
			\Base::instance()->set('cat', $category); // Documentation problem: copyFrom doesn't accept an array
			$this->categories[] = (new \v1l85\Models\Category())->copyFrom('cat');
			$this->category_list[] = $category['category_id']; 
			\Base::instance()->clear('cat');
		}

		return $this;
	}

	public function withImages(){
		$image = \Base::instance()->get('container')->make('v1l85\Repositories\ProductImageRepositoryInterface');
		$this->images = $image->getManyBy('product_id', $this->product_id);

		$default = $image->getById($this->default_image_id );
		$this->default_image = !$default->dry()?$default->file:'no-image';
		
		return $this;
	}

	public function withDefaultImage(){
		$image = \Base::instance()->get('container')->make('v1l85\Repositories\ProductImageRepositoryInterface');
		
		$default = $image->getById($this->default_image_id );
		$this->default_image = !$default->dry()?$default->file:'no-image';

		return $this;
	}

}
