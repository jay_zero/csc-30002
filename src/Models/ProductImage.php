<?php


namespace v1l85\Models;

class ProductImage extends F3BaseModel{
	
	protected $table = 'product_images';

	public $primary_key = 'product_image_id';

	public $fillable = ['file', 'product_id', 'order', 'caption'];
}
