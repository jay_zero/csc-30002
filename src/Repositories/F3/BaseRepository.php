<?php

namespace v1l85\Repositories\F3;

abstract class BaseRepository{

	protected $model;

	protected $sort;

	protected $sort_dir = 'ASC';

	protected $limit = 0;

	public function all(){
		return $this->model->find([], ['order' => $this->getSort(), 'limit' => $this->limit?:null]);
	}

	public function getById( $id ){
		return $this->model->load( [ $this->model->primary_key . '=?', $id] )?:$this->model;
	}

	public function getFirstBy( $field, $key ){
		return $this->model->load( [ $field . '=?', $key] )?:$this->model;
	}

	public function getManyBy( $field, $key ){
		return $this->model->find( [$field . '=?', $key], ['order' => $this->getSort(), 'limit' => $this->limit?:null] );
	}

	public function searchManyBy( $field, $key ){
		return $this->model->find( [$field . ' LIKE ?', '%'.$key.'%'], ['order' => $this->getSort(), 'limit' => $this->limit?:null] );
	}

	public function sort( $field, $direction = 'ASC' ){
		$this->sort = $field;
		$this->sort_dir = $direction;

		return $this;
	}

	public function deleteById( $id ){
			
		if( $record = $this->getById( $id ) ){
			return $record->erase();
		}else{ return false; }
		
	}

	public function create( $data = 'POST' ){
		//Simple method for quickly populating the db
		$this->model->reset();
		$this->model->copyFrom($data, function( $var ){
			return $this->assignmentFilter( $var );
		});
        return $this->model->save();
	}

	public function save( $id ){
		$record = $this->getById( $id );
		$record->copyFrom('POST', function( $var ){
			return $this->assignmentFilter( $var );
		});
		$record->update();

		return $record;
	}

	public function limit( $count = 0 ){
		$this->limit = $count;
		return $this;
	}


	private function getSort(){
		return ($this->sort?($this->sort . ' ' . $this->sort_dir):($this->model->primary_key . ' ' . $this->sort_dir));
	}

	private function assignmentFilter( $var ){
		return array_intersect_key($var, array_flip($this->model->fillable)); // Restrict only "fillable" fields to mass assignment from forms
	}

}