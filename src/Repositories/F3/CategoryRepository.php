<?php

namespace v1l85\Repositories\F3;

use \v1l85\Models\Category;

class CategoryRepository extends BaseRepository implements \v1l85\Repositories\CategoryRepositoryInterface{
	
	public function __construct(){
		$this->model = new Category;
	}

}