<?php

namespace v1l85\Repositories\F3;

use \v1l85\Models\Customer;

class CustomerRepository extends BaseRepository implements \v1l85\Repositories\CustomerRepositoryInterface{
	
	public function __construct(){
		$this->model = new Customer;
	}

	public function count(){
		return \Base::instance()->get('db')->exec('SELECT COUNT(*) as total FROM `customers`');
	}

	public function create( $data = 'POST' ){
		$f3 = \Base::instance();
		$f3->set($data.'.password', md5($f3->get($data.'.password')));
		return parent::create( $data );
	}

	public function save( $id ){
		$f3 = \Base::instance();
		$f3->set('POST.password', md5($f3->get('POST.password')));
		return parent::save( $id );
	}

}