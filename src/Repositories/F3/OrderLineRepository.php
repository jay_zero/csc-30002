<?php

namespace v1l85\Repositories\F3;

use \v1l85\Models\OrderLine;

class OrderLineRepository extends BaseRepository implements \v1l85\Repositories\OrderLineRepositoryInterface{
	
	public function __construct(){
		$this->model = new OrderLine;
	}

}