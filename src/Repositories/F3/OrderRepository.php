<?php

namespace v1l85\Repositories\F3;

use \v1l85\Models\Order;

class OrderRepository extends BaseRepository implements \v1l85\Repositories\OrderRepositoryInterface{
	
	public function __construct(){
		$this->model = new Order;
	}

	public function getOrdersByDate(){

		$orders = \Base::instance()->get('db')->exec("
		 	SELECT date(date_placed, 'unixepoch') as date, SUM(order_total) as total, date_placed as time
		 		FROM `orders`
		 		WHERE 
		 			date_placed >= ".strtotime(date('Y-m-01',strtotime('this month')))."
		 			AND date_placed < ".strtotime(date('Y-m-01',strtotime('next month')))."
		 		GROUP BY date
		 		ORDER BY date ASC");

		$sales = [];
		
		// Probably would be better as a SQL query except we're using SQLite 
		// Credit: http://stackoverflow.com/questions/15057259/display-every-day-of-the-month-even-if-no-data-is-available

		$start  = new \DateTime('first day of this month');
		$end    = new \DateTime('first day of this month + 1 month');
		$period = new \DatePeriod($start, new \DateInterval('P1D'), $end);

		foreach($period as $day){
			$sales[ strtotime($day->format('Y-m-d')) ] = 0;
		}

		foreach( $orders as $order ){
			$sales[ strtotime(date("Y-m-d", $order['time'])) ] = $order['total'];
		}

		return $sales;
	}

	public function getTotalOrders(){
		return \Base::instance()->get('db')->exec("SELECT COUNT(*) as num, SUM(order_total) as total FROM `orders`");
	}

}