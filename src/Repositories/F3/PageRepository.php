<?php

namespace v1l85\Repositories\F3;

use \v1l85\Models\Page;

class PageRepository extends BaseRepository implements \v1l85\Repositories\PageRepositoryInterface{
	
	public function __construct(){
		$this->model = new Page;
	}

}