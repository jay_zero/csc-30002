<?php

namespace v1l85\Repositories\F3;

use \v1l85\Models\ProductImage;

class ProductImageRepository extends BaseRepository implements \v1l85\Repositories\ProductImageRepositoryInterface{
	
	public function __construct(){
		$this->model = new ProductImage;
	}

}