<?php

namespace v1l85\Repositories\F3;

use \v1l85\Models\Product;

class ProductRepository extends BaseRepository implements \v1l85\Repositories\ProductRepositoryInterface{

	protected $db;
	
	public function __construct(){
		$this->model = new Product;
		$this->db = \Base::instance()->get('db');
	}

	/**
	 * Override the base repository create function to include additional facility to synchronise categories and product images
	 * 
	 * @param  string $data String identifier of a Hive Array of information to be copied
	 * @return v1l85\Model\Product 
	 */
	public function create( $data = 'POST' ){
		$product = parent::create( $data );

		$this->syncCategories( $product->product_id );
		$product->default_image_id = $this->syncImages( $product->product_id );
		$product->save();

		return $product;
	}

	/**
	 * Override the base repository save function to include additional facility to synchronise categories and product images
	 * 
	 * @param  int $id Product Identifier
	 * @return v1l85\Model\Product 
	 */
	public function save( $id ){
		$product = parent::save( $id );

		$this->syncCategories( $product->product_id );
		$product->default_image_id = $this->syncImages( $product->product_id );
		$product->save();

		return $product;
	}

	/**
	 * Upload and Synchronise product images.  It would be better for this to be in the ProductImageRepository and perhaps have an integrating service
	 * 
	 * @param  int $product_id Product Identifier
	 * @return int             Default value for the product's default image
	 */
	private function syncImages( $product_id ){
		
		$f3 = \Base::instance();

		$default = $f3->get('POST.default_image');

		//Sync Image files
		foreach( $_FILES['image']['name'] as $id => $name ){

			//if a file has been submitted
			if( !empty( $name ) ){

				// Remove exsiting product image links rather than updating
				if( $id > 0 ){
					$this->db->exec( 'DELETE FROM `product_images` WHERE product_image_id=?', $id );
				}

				// Create a new image from the temporary uploaded file - will fail on no image provided.  Error checking / handling needed.
				$img = new \Image($_FILES['image']['tmp_name'][$id], false, '/');	


				// Resize and save 3 versions of the image
				$file = \Base::instance()->hash( $_FILES['image']['name'][$id] );
				$file_path = $f3->get('ROOT') . '/assets/images/products/';

				// This should really be a more generic function such that an image of any size can be created
				// Perhaps if work continued on this project, a function / method / service will be written for this purpose
				// and this will be refactored.

				// "Zoom Image"
				$img->resize(1000,1000);
				$myfile = fopen( $file_path . $file . '.jpg', "w");
				fwrite($myfile, $img->dump('jpeg'));
				fclose( $myfile );
				$img->restore();

				//Regular Thumb
				$img->resize(450,450);

				$myfile = fopen( $file_path . $file . '_450x450.jpg', "w");
				fwrite($myfile, $img->dump('jpeg'));
				fclose( $myfile );

				//Small thumb
				$img->resize(100,100);

				$myfile = fopen( $file_path . $file . '_100x100.jpg', "w");
				fwrite($myfile, $img->dump('jpeg'));
				fclose( $myfile );
				// End writing the image sizes


				//Save thsee to the database
				$this->db->exec(
					'INSERT INTO product_images (product_id, file, caption) VALUES (:id,:file,:caption)', 
					[':id'=>$product_id, ':file'=>$file, ':caption'=>$_POST['caption'][$id]?:'']
				);

				if( $default == $id ){
					$default = $this->db->lastInsertId();
				}

			}
		}

		return $default;
	}

	/**
	 * Synchronise a product with the categories it is to be listed under
	 * 
	 * @param  int $product_id
	 * @return \v1l85\Repositories\F3\ProductRepository
	 */
	private function syncCategories($product_id){

		//Sync products to categories using the specified link table
		$this->db->exec( 'DELETE FROM `products_categories` WHERE product_id=?', $product_id);
		foreach( \Base::Instance()->get('POST.category_id') as $category_id ){
			$this->db->exec(
				'INSERT INTO products_categories (product_id, category_id) VALUES (:id,:cat)', 
				[':id'=>$product_id, ':cat'=>$category_id]
			);
		}

		return $this;
	}


}