<?php

namespace v1l85\Repositories;

interface OrderRepositoryInterface{

	public function getOrdersByDate();
	
}