<?php

namespace v1l85\Template;

//Just extend the base template from F3.

class Template extends \Template {

	public function currency( $val ){
		return '&pound;' . number_format(((int)$val / 100),2);
	}
}