<?php

namespace v1l85\Validators;

class AddressValidator extends BaseValidator{

	protected $rules = [
		"line_1"	=> [
			"label"	=>	'Address Line 1',
			'rules' =>	[
				'required', 
				'minLength'	 	=> 	2
			]
		],
		"postcode"	=> [
			"label"	=>	'Postcode',
			'rules' =>	[
				'required'
			]
		],
		"city"	=> [
			"label"	=>	'City',
			'rules' =>	[
				'required'
			]
		],
	];
	
}