<?php

namespace v1l85\Validators;

abstract class BaseValidator{
	
	protected $rules = [];
	protected $errors = [];


	public function validate( $data ){

		//Check for required fields
		foreach( $this->rules as $field => $options ){
			foreach( $options['rules'] as $rule => $opt ){
				if( $opt == "required" ){
					if( !isset( $data[$field]) || empty( $data[$field] ) ){
						$this->errors[] = "The field <strong>". $options['label']."</strong> is required.";
					}
				}
			}
		}

		foreach( $data as $field => $value ){

			//If there are rules set for the current field
			if( isset( $this->rules[$field] ) && !empty( $value ) ){
				
				//For each of the given rules, validate if possible
				foreach( $this->rules[$field]['rules'] as $rule => $opts ){
					if( $opts == 'required' ) continue; //We've dealt with required fields at this point.
					
					$method = 'validate'.ucfirst( is_int($rule)?$opts:$rule );

					if( method_exists( $this, $method ) ){
						$this->{$method}($field, $value);
					}else{
						throw new \v1l85\Exceptions\InvalidValidationRuleException("Validation rule, ".$method.' not defined.' );
					}
				}

			}

		}

		if( $this->errors ) return false;
		else return true;

	}

	public function getErrors(){
		return $this->errors;
	}

	private function validateEmail( $field, $value ){
		if( !filter_var( $value, FILTER_VALIDATE_EMAIL ) ){
			$this->errors[] = "The field <strong>".$this->rules[$field]['label']."</strong> should be a valid email address.";
		}
	}

	private function validateUrl( $field, $value ){
		if( !filter_var( $value, FILTER_VALIDATE_URL ) ){
			$this->errors[] = "The field <strong>".$this->rules[$field]['label']."</strong> should be a valid URL.";
		}
	}

	private function validateInteger( $field, $value ){
		if( filter_var( $value, FILTER_VALIDATE_INT )!==0 && !filter_var( $value, FILTER_VALIDATE_INT ) ){
			$this->errors[] = "The field <strong>".$this->rules[$field]['label']."</strong> should be a valid whole number.";
		}
	}

	private function validateSlug( $field, $value ){
		//Simple slug validation
		if( !preg_match( '/^[a-z][-a-z0-9]*$/', $value) ){
			$this->errors[] = "The field <strong>".$this->rules[$field]['label']."</strong> should be a valid SEO URL as described.";
		}
	}

	private function validateMinLength( $field, $value ){
		$length = $this->rules[$field]['rules']['minLength'];
		if( strlen($value) < $length ){
			$this->errors[] = "The field <strong>".$this->rules[$field]['label']."</strong> should be a minimum of " . $length . " characters";
		}
	}


}