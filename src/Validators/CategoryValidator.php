<?php

namespace v1l85\Validators;

class CategoryValidator extends BaseValidator{

	protected $rules = [
		"name"	=> [
			"label"	=>	'Category Name',
			'rules' =>	[
				'required', 
				'minLength'	 	=> 	5 
			]
		],
		"parent_id"	=> [
			"label"	=>	'Parent Category',
			'rules' =>	[
				'integer',
				// 'exists'	=>	'categories'
			]
		],
		"slug"	=> [
			"label"	=>	'SEO Friendly URL',
			'rules' =>	[
				'required', 
				'minLength' => 5,
				'slug' 
			]
		]
	];
	
}