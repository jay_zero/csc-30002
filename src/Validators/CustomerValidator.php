<?php

namespace v1l85\Validators;

class CustomerValidator extends BaseValidator{

	protected $rules = [
		"firstname"	=> [
			"label"	=>	'First Name',
			'rules' =>	[
				'required', 
				'minLength'	 	=> 	2
			]
		],
		"lastname"	=> [
			"label"	=>	'Last Name',
			'rules' =>	[
				'required', 
				'minLength'	 	=> 	2
			]
		],
		"password"	=> [
			"label"	=>	'Password',
			'rules' =>	[
				'required'
			]
		],
		"email"	=> [
			"label"	=>	'Email',
			'rules' =>	[
				'required',
				'email'
			]
		],
	];
	
}