<?php

namespace v1l85\Validators;

class PageValidator extends BaseValidator{

	protected $rules = [
		"name"	=> [
			"label"	=>	'Category Name',
			'rules' =>	[
				'required', 
				'minLength'	 	=> 	5 
			]
		],
		"slug"	=> [
			"label"	=>	'SEO Friendly URL',
			'rules' =>	[
				'required', 
				'minLength' => 5,
				'slug' 
			]
		],
		"content"	=> [
			"label"	=>	'Page Content',
			'rules' =>	[
				'required'
			]
		]
	];
	
}