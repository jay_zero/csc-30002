<?php

namespace v1l85\Validators;

class ProductValidator extends BaseValidator{

	protected $rules = [
		"name"	=> [
			"label"	=>	'Product Name',
			'rules' =>	[
				'required', 
				'minLength'	 	=> 	5 
			]
		],
		"parent_id"	=> [
			"label"	=>	'Parent Category',
			'rules' =>	[
				'integer',
				// 'exists'	=>	'categories'
			]
		],
		"slug"	=> [
			"label"	=>	'SEO Friendly URL',
			'rules' =>	[
				'required', 
				'minLength' => 5,
				'slug' 
			]
		],
		"price"	=>	[
			'label'	=>	'Price',
			'rules'	=>	[
				'required',
				'integer'
			]
		],
		"special_price"	=>	[
			'label'	=>	'Special Price',
			'rules'	=>	[
				'integer'
			]
		],
		"stock_level"	=>	[
			'label'	=>	'Stock Level',
			'rules'	=>	[
				'integer'
			]
		],
		"out_of_stock_status"	=>	[
			'label'	=>	'Out of Stock Status',
			'rules'	=>	[
				'integer'
			]
		],
		'default_image'	=>	[
			'label'	=>	'Default Image',
			'rules'	=>	[
				'required'
			]
		]
	];
	
}