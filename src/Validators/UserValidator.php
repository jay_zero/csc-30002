<?php

namespace v1l85\Validators;

class UserValidator extends BaseValidator{

	protected $rules = [
		"username"	=> [
			"label"	=>	'Username',
			'rules' =>	[
				'required', 
				'minLength'	 	=> 	2
			]
		],
		"password"	=> [
			"label"	=>	'Password',
			'rules' =>	[
				'required'
			]
		],
		"email"	=> [
			"label"	=>	'Email',
			'rules' =>	[
				'required',
				'email'
			]
		],
	];
	
}