<img src="http://placehold.it/1200x200" style="margin-bottom: 40px;" />

<aside class="sidebar">

<?php if ($children): ?>
	<div class="widget">
		<h2 class="widget--title"><?php echo $category['name']; ?></h2>
		<div class="widget--body">
			<ul >
				<?php foreach (($children?:array()) as $child): ?>
					<li><a href="/category/<?php echo $child->slug; ?>"><?php echo $child->name; ?></a></li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
<?php endif; ?>

	<div class="widget">
		<h2 class="widget--title">Top Sellers</h2>
		<div class="widget--body">
			
		</div>
	</div>

	<div class="widget">
		<h2 class="widget--title">Another Block</h2>
		<div class="widget--body">
			<p>Integer facilisis justo leo, ut rutrum mauris congue nec. Quisque suscipit lorem eu pellentesque interdum.</p>
		</div>
	</div>

	<img src="http://placehold.it/450x450&text=upsell" />
</aside>



<div class="primary-content">

	<h1 class="category-title"><?php echo $category['name']; ?></h1>
	<p><?php echo $category['description']; ?></p>

	<div class="paging">
		<?php echo count($category['products']); ?> Product(s)
	</div>

	<?php if ($category['products']): ?>
		
		<section class="product--grid">

			<?php foreach (($category['products']?:array()) as $product): ?>
				<div class="product--grid_box">
				<div class="product--detail_basket">
					<form action="/basket/add" method="post">
						<input type="hidden" name="qty" value="1" />
						<input type="hidden" name="product_id" value="<?php echo $product['product_id']; ?>" />
					<div class="image">
						<a href="<?php echo $this->alias('product', 'slug='.$product['slug']); ?>">
							<img src="/assets/images/products/<?php echo $product['default_image']; ?>_450x450.jpg" />
							<?php if ($product['special_price'] != 0): ?>
							<span class="badge sale">Sale</span>
						<?php endif; ?>
						<span class="badge new">New</span>
							<span class="image--overlay"></span>

						</a>		
						<div class="image--buttons">
							<a href="<?php echo $this->alias('product', 'slug='.$product['slug']); ?>" class="quick-look"><i class="fa fa-search"></i></a>
							<a href="#" class="btn-basket" data-id="<?php echo $product['product_id']; ?>"><i class="fa fa-shopping-cart"></i></a>
						</div>

					</div>
					<h3 class="name"><a href="<?php echo $this->alias('product', 'slug='.$product['slug']); ?>"><?php echo $product['name']; ?></a></h3>
					<div class="price">
						<?php if ($product['special_price'] != 0): ?>
							
								<?php echo $this->currency($product['special_price'] * ($product['charge_vat']?1.2:1)); ?> <span><?php echo $this->currency($product['price'] * ($product['charge_vat']?1.2:1)); ?></span>
							
							<?php else: ?>
								<?php echo $this->currency($product['price'] * ($product['charge_vat']?1.2:1)); ?>
							
						<?php endif; ?>
					</div>
					</form>
					</div>
				</div>
			<?php endforeach; ?>

		</section>
		
		<?php else: ?>
			<div class="notice error">There are no products in this category just yet!  Please check back soon!</div>
		
	<?php endif; ?>

</div>